package fr.univlr.dsi.fwknotifgui.ctrl;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import fr.univlr.dsi.fwknotif.server.metier.EONotificationMailConf;
import fr.univlr.dsi.fwknotif.server.metier.EONotificationUserPreferences;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;
import fr.univlr.dsi.fwknotifgui.components.Adresse;
import fr.univlr.dsi.fwknotifgui.components.EOPersonneAdresseBook;
import fr.univlr.dsi.fwknotifgui.components.PreferencesUtils;

public class AddressBookCtrl extends ANotifComponentCtrl {

	public AddressBookCtrl(ANotifComponent component) {
		super(component);
		this.displayPhoto = false;

		if (individu != null) {
			try {
				fillAdresses();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		this.displayNotifcationSMSStatus = displayNotificationCheckbox();
	}

	private final static Logger log = Logger.getLogger(AddressBookCtrl.class);
	private EOEditingContext ec = session().defaultEditingContext();
	private EOIndividu individu;

	private NSArray<EORepartPersonneAdresse> adresses = new NSArray<EORepartPersonneAdresse>();
	private EORepartPersonneAdresse adresse;

	private NSArray<EOPersonneTelephone> telephones = new NSArray<EOPersonneTelephone>();
	private EOPersonneTelephone telephone;

	public Boolean displayPhoto;
	private String codeValidation = null;
	private Boolean codeValidationStatus = null;
	private Boolean displayNotifcationSMSStatus = null;
	private EOPersonneAdresseBook eoPersonneAdresseBook = null;
	private Adresse eoAdresse = null;
	private NSMutableArray<EOVlans> listeVlan = new NSMutableArray<EOVlans>();
	private EOVlans vlan = null;

	public Adresse getEoAdresse() {
		return eoAdresse;
	}

	public void setEoAdresse(Adresse eoAdresse) {
		this.eoAdresse = eoAdresse;
	}

	public EOPersonneAdresseBook getEoPersonneAdresseBook() {
		return eoPersonneAdresseBook;
	}

	public void setEoPersonneAdresseBook(EOPersonneAdresseBook eoPersonneAdresseBook) {
		this.eoPersonneAdresseBook = eoPersonneAdresseBook;
	}

	public void setAdresses(NSArray<EORepartPersonneAdresse> adresses) {
		this.adresses = adresses;
	}

	public EORepartPersonneAdresse getAdresse() {
		return adresse;
	}

	public void setAdresse(EORepartPersonneAdresse adresse) {
		this.adresse = adresse;
	}

	public String getCodeValidation() {
		return codeValidation;
	}

	public void setCodeValidation(String codeValidation) {
		this.codeValidation = codeValidation;
	}

	public Boolean getCodeValidationStatus() {
		return codeValidationStatus;
	}

	public void setCodeValidationStatus(Boolean codeValidationStatus) {
		this.codeValidationStatus = codeValidationStatus;
	}

	public Boolean getDisplayNotifcationSMSStatus() {
		return displayNotifcationSMSStatus;
	}

	public void setDisplayNotifcationSMSStatus(Boolean displayNotifcationSMSStatus) {
		this.displayNotifcationSMSStatus = displayNotifcationSMSStatus;
	}

	public NSMutableArray<EOVlans> getListeVlan() {
		return listeVlan;
	}

	public void setListeVlan(NSMutableArray<EOVlans> listeVlan) {
		this.listeVlan = listeVlan;
	}

	public EOVlans getVlan() {
		return vlan;
	}

	public void setVlan(EOVlans vlan) {
		this.vlan = vlan;
	}

	public EOIndividu getIndividu() {
		return individu;
	}

	public void setIndividu(EOIndividu individu) {
		this.individu = individu;
		fillAdresses();
		fillVLan();
	}

	public EOPersonneTelephone getTelephone() {
		return telephone;
	}

	public void setTelephone(EOPersonneTelephone telephone) {
		this.telephone = telephone;
	}

	/**
	 * DEUB DES VRAIES FONCTIONS
	 */

	private void fillVLan() {

		listeVlan = new NSMutableArray<EOVlans>();

		if (individu != null) {
			NSArray<EOCompte> comptes = individu.toComptes();

			for (EOCompte compte : comptes) {
				EOVlans vlan = compte.toVlans();
				if (vlan != null) {
					listeVlan.add(vlan);
				}
			}
		}

	}

	public NSMutableArray<Adresse> fillAdresses() {

		if (individu != null) {
			EOQualifier qual1 = EORepartPersonneAdresse.PERS_ID.eq(individu.persId());
			EOQualifier qual2 = null;

			if (individu.isEtudiant()) {
				qual2 = EORepartPersonneAdresse.TADR_CODE.eq("ETUD");
			} else {
				if (individu.isPersonnel()) {
					qual2 = EORepartPersonneAdresse.TADR_CODE.eq("PERSO");
				} else {
					if (individu.isDoctorant()) {

						NSMutableArray<String> codes = new NSMutableArray<String>();
						codes.add("ETUD");
						codes.add("PERSO");
						qual2 = EORepartPersonneAdresse.TADR_CODE.in(codes);
					}
				}
			}

			EOQualifier qualifierFinale = ERXQ.and(qual1, qual2);
			log.info("Adresses : qualifierFinale : " + qualifierFinale);
			adresses = EORepartPersonneAdresse.fetchAll(ec, qualifierFinale);
		}

		eoPersonneAdresseBook = new EOPersonneAdresseBook(individu);

		for (EORepartPersonneAdresse add : adresses) {

			EONotificationMailConf mailConf = getMailConf(add);

			Boolean status = false;

			if (mailConf != null) {
				if (mailConf.mailConfValideStatus() != null) {
					if (mailConf.mailConfValideStatus().equalsIgnoreCase("O")) {
						status = true;
					}
				}

			}
			eoPersonneAdresseBook.addAdresse(new Adresse(add.typeAdresse(), add.eMail(), mailConf.mailAValider(),
					mailConf.mailValide(), status, mailConf.dModification(), mailConf.mailConfCodeHash()));
		}

		return eoPersonneAdresseBook.getAdresses();
	}

	public NSMutableArray<Adresse> getAdresses() {

		if (eoPersonneAdresseBook != null) {
			if (eoPersonneAdresseBook.getAdresses() != null) {
				return eoPersonneAdresseBook.getAdresses();
			}
		} else {
			return null;
		}
		return eoPersonneAdresseBook.getAdresses();
	}

	public EONotificationMailConf getMailConf(EORepartPersonneAdresse personneAdresse) {

		EOQualifier qual1 = EONotificationMailConf.MAIL_CONF_PERS_ID.eq(individu.persId());
		EOQualifier qual2 = EONotificationMailConf.MAIL_CONF_TADR_CODE.eq(personneAdresse.tadrCode());
		EOQualifier qual3 = EONotificationMailConf.MAIL_CONF_ADR_ORDRE.eq(personneAdresse.adrOrdre());
		EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);
		EONotificationMailConf mailConf = EONotificationMailConf.fetchNotificationMailConf(ec, qualifierFinale);

		return mailConf;
	}

	public NSArray<EOPersonneTelephone> getTelephones() {

		if (individu != null) {

			EOQualifier qual1 = EOPersonneTelephone.PERS_ID.eq(individu.persId());
			EOQualifier qual2 = EOPersonneTelephone.TYPE_NO.eq("MOB");
			EOQualifier qual3 = null;

			if (individu.isEtudiant()) {

				qual3 = EOPersonneTelephone.TYPE_TEL.eq("ETUD");
			} else {
				if (individu.isPersonnel()) {
					NSMutableArray<String> codes = new NSMutableArray<String>();
					codes.add("PRF");
					codes.add("PRV");
					qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
				} else {
					if (individu.isDoctorant()) {
						NSMutableArray<String> codes = new NSMutableArray<String>();
						codes.add("ETUD");
						codes.add("PRF");
						codes.add("PRV");
						qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
					}
				}
			}

			EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);
			try {
				telephones = EOPersonneTelephone.fetchAll(ec, qualifierFinale);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return telephones;
	}

	public void setTelephones(NSArray<EOPersonneTelephone> telephones) {
		this.telephones = telephones;
	}

	public Boolean getDisplayPhoto() {

		if (this.individu.indPhoto().equalsIgnoreCase("O")) {
			displayPhoto = true;
		}

		return displayPhoto;
	}

	public void setDisplayPhoto(Boolean displayPhoto) {
		this.displayPhoto = displayPhoto;
	}

	public Boolean hasPhones() {
		Boolean rep = false;

		if (telephones.count() > 0) {
			rep = true;
		}

		return rep;
	}

	public Boolean displayNoPhones() {

		Boolean rep = false;

		if (individu != null && telephones.count() == 0) {
			rep = true;
		}

		return rep;
	}

	public Boolean getNotification() {

		Boolean rep = false;

		EONotificationUserPreferences n = null;

		if (individu != null) {
			EOQualifier qual1 = EONotificationUserPreferences.USER_PREF_PERS_ID.eq(individu.persId());

			try {
				n = EONotificationUserPreferences.fetchNotificationUserPref(ec, qual1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (n != null) {
				if (n.userPrefNotifEnable().equalsIgnoreCase("O")) {
					rep = true;
				} else {
					rep = false;
				}
			} else {
				rep = false;
			}
		}

		return rep;

	}

	public Boolean displayNotificationCheckbox() {

		Boolean rep = false;

		PreferencesUtils pref = new PreferencesUtils(ec);

		if (pref.getNotificationCheckBox().stringContent().equalsIgnoreCase("O")) {
			rep = true;
		}

		return rep;
	}

}
