// DO NOT EDIT.  Make changes to EONotificationMailConf.java instead.
package fr.univlr.dsi.fwknotif.server.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import java.math.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.cocktail.dt.server.metier.A_FwkDTRecord;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONotificationMailConf extends  A_FwkDTRecord {
  public static final String ENTITY_NAME = "NotificationMailConf";
	public static final String ENTITY_TABLE_NAME = "HISTOTOX.NOTIFICATION_MAIL_CONF";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> MAIL_A_VALIDER = new ERXKey<String>("mailAValider");
  public static final ERXKey<Integer> MAIL_CONF_ADR_ORDRE = new ERXKey<Integer>("mailConfAdrOrdre");
  public static final ERXKey<String> MAIL_CONF_CODE_HASH = new ERXKey<String>("mailConfCodeHash");
  public static final ERXKey<Integer> MAIL_CONF_PERS_ID = new ERXKey<Integer>("mailConfPersId");
  public static final ERXKey<String> MAIL_CONF_TADR_CODE = new ERXKey<String>("mailConfTadrCode");
  public static final ERXKey<String> MAIL_CONF_VALIDE_STATUS = new ERXKey<String>("mailConfValideStatus");
  public static final ERXKey<String> MAIL_VALIDE = new ERXKey<String>("mailValide");
  public static final ERXKey<String> UUID = new ERXKey<String>("uuid");
  public static final ERXKey<NSTimestamp> UUID_D_DEBUT = new ERXKey<NSTimestamp>("uuidDDebut");
  public static final ERXKey<NSTimestamp> UUID_D_FIN = new ERXKey<NSTimestamp>("uuidDFin");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> TO_REPART_PERSONNE_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>("to_repartPersonneAdresse");

  // Attributes
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String MAIL_A_VALIDER_KEY = MAIL_A_VALIDER.key();
  public static final String MAIL_CONF_ADR_ORDRE_KEY = MAIL_CONF_ADR_ORDRE.key();
  public static final String MAIL_CONF_CODE_HASH_KEY = MAIL_CONF_CODE_HASH.key();
  public static final String MAIL_CONF_PERS_ID_KEY = MAIL_CONF_PERS_ID.key();
  public static final String MAIL_CONF_TADR_CODE_KEY = MAIL_CONF_TADR_CODE.key();
  public static final String MAIL_CONF_VALIDE_STATUS_KEY = MAIL_CONF_VALIDE_STATUS.key();
  public static final String MAIL_VALIDE_KEY = MAIL_VALIDE.key();
  public static final String UUID_KEY = UUID.key();
  public static final String UUID_D_DEBUT_KEY = UUID_D_DEBUT.key();
  public static final String UUID_D_FIN_KEY = UUID_D_FIN.key();
  // Relationships
  public static final String TO_REPART_PERSONNE_ADRESSE_KEY = TO_REPART_PERSONNE_ADRESSE.key();

  private static Logger LOG = Logger.getLogger(_EONotificationMailConf.class);

  public EONotificationMailConf localInstanceIn(EOEditingContext editingContext) {
    EONotificationMailConf localInstance = (EONotificationMailConf)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EONotificationMailConf.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.D_MODIFICATION_KEY);
  }

  public String mailAValider() {
    return (String) storedValueForKey(_EONotificationMailConf.MAIL_A_VALIDER_KEY);
  }

  public void setMailAValider(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailAValider from " + mailAValider() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_A_VALIDER_KEY);
  }

  public Integer mailConfAdrOrdre() {
    return (Integer) storedValueForKey(_EONotificationMailConf.MAIL_CONF_ADR_ORDRE_KEY);
  }

  public void setMailConfAdrOrdre(Integer value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailConfAdrOrdre from " + mailConfAdrOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_CONF_ADR_ORDRE_KEY);
  }

  public String mailConfCodeHash() {
    return (String) storedValueForKey(_EONotificationMailConf.MAIL_CONF_CODE_HASH_KEY);
  }

  public void setMailConfCodeHash(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailConfCodeHash from " + mailConfCodeHash() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_CONF_CODE_HASH_KEY);
  }

  public Integer mailConfPersId() {
    return (Integer) storedValueForKey(_EONotificationMailConf.MAIL_CONF_PERS_ID_KEY);
  }

  public void setMailConfPersId(Integer value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailConfPersId from " + mailConfPersId() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_CONF_PERS_ID_KEY);
  }

  public String mailConfTadrCode() {
    return (String) storedValueForKey(_EONotificationMailConf.MAIL_CONF_TADR_CODE_KEY);
  }

  public void setMailConfTadrCode(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailConfTadrCode from " + mailConfTadrCode() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_CONF_TADR_CODE_KEY);
  }

  public String mailConfValideStatus() {
    return (String) storedValueForKey(_EONotificationMailConf.MAIL_CONF_VALIDE_STATUS_KEY);
  }

  public void setMailConfValideStatus(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailConfValideStatus from " + mailConfValideStatus() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_CONF_VALIDE_STATUS_KEY);
  }

  public String mailValide() {
    return (String) storedValueForKey(_EONotificationMailConf.MAIL_VALIDE_KEY);
  }

  public void setMailValide(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating mailValide from " + mailValide() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.MAIL_VALIDE_KEY);
  }

  public String uuid() {
    return (String) storedValueForKey(_EONotificationMailConf.UUID_KEY);
  }

  public void setUuid(String value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating uuid from " + uuid() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.UUID_KEY);
  }

  public NSTimestamp uuidDDebut() {
    return (NSTimestamp) storedValueForKey(_EONotificationMailConf.UUID_D_DEBUT_KEY);
  }

  public void setUuidDDebut(NSTimestamp value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating uuidDDebut from " + uuidDDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.UUID_D_DEBUT_KEY);
  }

  public NSTimestamp uuidDFin() {
    return (NSTimestamp) storedValueForKey(_EONotificationMailConf.UUID_D_FIN_KEY);
  }

  public void setUuidDFin(NSTimestamp value) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
    	_EONotificationMailConf.LOG.debug( "updating uuidDFin from " + uuidDFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationMailConf.UUID_D_FIN_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> to_repartPersonneAdresse() {
    return (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)storedValueForKey(_EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> to_repartPersonneAdresse(EOQualifier qualifier) {
    return to_repartPersonneAdresse(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> to_repartPersonneAdresse(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> results;
      results = to_repartPersonneAdresse();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTo_repartPersonneAdresse(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    includeObjectIntoPropertyWithKey(object, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
  }

  public void removeFromTo_repartPersonneAdresse(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    excludeObjectFromPropertyWithKey(object, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
  }

  public void addToTo_repartPersonneAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
      _EONotificationMailConf.LOG.debug("adding " + object + " to to_repartPersonneAdresse relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToTo_repartPersonneAdresse(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
    }
  }

  public void removeFromTo_repartPersonneAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    if (_EONotificationMailConf.LOG.isDebugEnabled()) {
      _EONotificationMailConf.LOG.debug("removing " + object + " from to_repartPersonneAdresse relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromTo_repartPersonneAdresse(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
    }
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse createTo_repartPersonneAdresseRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
    return (org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse) eo;
  }

  public void deleteTo_repartPersonneAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EONotificationMailConf.TO_REPART_PERSONNE_ADRESSE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTo_repartPersonneAdresseRelationships() {
    Enumeration<org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse> objects = to_repartPersonneAdresse().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTo_repartPersonneAdresseRelationship(objects.nextElement());
    }
  }


  public static EONotificationMailConf createNotificationMailConf(EOEditingContext editingContext, Integer mailConfAdrOrdre
, Integer mailConfPersId
, String mailConfTadrCode
) {
    EONotificationMailConf eo = (EONotificationMailConf) EOUtilities.createAndInsertInstance(editingContext, _EONotificationMailConf.ENTITY_NAME);    
		eo.setMailConfAdrOrdre(mailConfAdrOrdre);
		eo.setMailConfPersId(mailConfPersId);
		eo.setMailConfTadrCode(mailConfTadrCode);
    return eo;
  }

  public static ERXFetchSpecification<EONotificationMailConf> fetchSpec() {
    return new ERXFetchSpecification<EONotificationMailConf>(_EONotificationMailConf.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONotificationMailConf> fetchAllNotificationMailConfs(EOEditingContext editingContext) {
    return _EONotificationMailConf.fetchAllNotificationMailConfs(editingContext, null);
  }

  public static NSArray<EONotificationMailConf> fetchAllNotificationMailConfs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONotificationMailConf.fetchNotificationMailConfs(editingContext, null, sortOrderings);
  }

  public static NSArray<EONotificationMailConf> fetchNotificationMailConfs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONotificationMailConf> fetchSpec = new ERXFetchSpecification<EONotificationMailConf>(_EONotificationMailConf.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONotificationMailConf> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONotificationMailConf fetchNotificationMailConf(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotificationMailConf.fetchNotificationMailConf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotificationMailConf fetchNotificationMailConf(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONotificationMailConf> eoObjects = _EONotificationMailConf.fetchNotificationMailConfs(editingContext, qualifier, null);
    EONotificationMailConf eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NotificationMailConf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotificationMailConf fetchRequiredNotificationMailConf(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotificationMailConf.fetchRequiredNotificationMailConf(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotificationMailConf fetchRequiredNotificationMailConf(EOEditingContext editingContext, EOQualifier qualifier) {
    EONotificationMailConf eoObject = _EONotificationMailConf.fetchNotificationMailConf(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NotificationMailConf that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotificationMailConf localInstanceIn(EOEditingContext editingContext, EONotificationMailConf eo) {
    EONotificationMailConf localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }


//Attributs non visibles
public static final String MAIL_CONF_ORDRE_KEY = "mailConfOrdre";
public static final ERXKey<Integer> MAIL_CONF_ORDRE = new ERXKey<Integer>("mailConfOrdre");

/**
 * Créer une instance de EONotificationMailConf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
//  public static  EONotificationMailConf createEONotificationMailConf(EOEditingContext editingContext//////////////////, Integer mailConfAdrOrdre
////////////////////, Integer mailConfPersId
////////////////, String mailConfTadrCode
//////////////////////////////			) {
//    EONotificationMailConf eo = (EONotificationMailConf) createAndInsertInstance(editingContext, _EONotificationMailConf.ENTITY_NAME);//    
////////////////////		eo.setMailConfAdrOrdre(mailConfAdrOrdre);
////////////////////		eo.setMailConfPersId(mailConfPersId);
////////////////		eo.setMailConfTadrCode(mailConfTadrCode);
//////////////////////////////    return eo;
//  }


}
