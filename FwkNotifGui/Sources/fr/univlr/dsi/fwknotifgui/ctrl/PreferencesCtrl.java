package fr.univlr.dsi.fwknotifgui.ctrl;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.dsi.fwknotif.server.metier.EONotifcationPreferences;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;
import fr.univlr.dsi.fwknotifgui.components.PreferencesUtils;

public class PreferencesCtrl extends ANotifComponentCtrl {

	private final static Logger log = Logger.getLogger(AddressBookCtrl.class);
	private EOEditingContext ec = session().defaultEditingContext();
	private EOIndividu connectedIndividu = EOIndividu.individuWithPersId(ec, session().applicationUser().getPersId());

	private EONotifcationPreferences mail;
	private EONotifcationPreferences subject;
	private EONotifcationPreferences from;
	private EONotifcationPreferences cc;

	

	private EONotifcationPreferences frenchTitle;
	private EONotifcationPreferences englishTitle;
	private EONotifcationPreferences frenchSubTitle;
	private EONotifcationPreferences englishSubTitle;
	
	
	private EONotifcationPreferences frenchExplication;
	private EONotifcationPreferences englishExplication;
	private EONotifcationPreferences forbbidenDomains;
	private EONotifcationPreferences maxNumber;
	private EONotifcationPreferences notificationObject;
	private EONotifcationPreferences nb_days_lifetime_link;
	private Boolean notification;
	private PreferencesUtils pref = null;

	public EONotifcationPreferences getFrenchTitle() {
		return frenchTitle;
	}

	public void setFrenchTitle(EONotifcationPreferences frenchTitle) {
		this.frenchTitle = frenchTitle;
	}

	public EONotifcationPreferences getEnglishTitle() {
		return englishTitle;
	}

	public void setEnglishTitle(EONotifcationPreferences englishTitle) {
		this.englishTitle = englishTitle;
	}

	public EONotifcationPreferences getFrenchSubTitle() {
		return frenchSubTitle;
	}

	public void setFrenchSubTitle(EONotifcationPreferences frenchSubTitle) {
		this.frenchSubTitle = frenchSubTitle;
	}

	public EONotifcationPreferences getEnglishSubTitle() {
		return englishSubTitle;
	}

	public void setEnglishSubTitle(EONotifcationPreferences englishSubTitle) {
		this.englishSubTitle = englishSubTitle;
	}

	public EONotifcationPreferences getMail() {
		return mail;
	}

	public void setMail(EONotifcationPreferences mail) {
		this.mail = mail;
	}

	public EONotifcationPreferences getSubject() {
		return subject;
	}

	public void setSubject(EONotifcationPreferences subject) {
		this.subject = subject;
	}

	public EONotifcationPreferences getFrom() {
		return from;
	}

	public void setFrom(EONotifcationPreferences from) {
		this.from = from;
	}

	public EONotifcationPreferences getCc() {
		return cc;
	}

	public void setCc(EONotifcationPreferences cc) {
		this.cc = cc;
	}

	public EONotifcationPreferences getForbbidenDomains() {
		return forbbidenDomains;
	}

	public PreferencesCtrl(ANotifComponent component) {
		super(component);

		this.pref = new PreferencesUtils(ec);

		frenchTitle = pref.getTitle("fr");
		englishTitle = pref.getTitle("en");
		
		frenchSubTitle = pref.getSubTitle("fr");
		englishSubTitle = pref.getSubTitle("en");
		
		
		mail = pref.getMail();
		from = pref.getFrom();
		subject = pref.getSubject();
		cc = pref.getCc();
		nb_days_lifetime_link = pref.getNb_days_lifetime_link();
		forbbidenDomains = pref.getDomainesInterditsString();

		frenchExplication = pref.getExplication("fr");
		englishExplication = pref.getExplication("en");
		maxNumber = getMaxNumber();
		notificationObject = getDisplayNotificationCheckbox();
	}

	public EONotifcationPreferences getMaxNumber() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("max_phones");
		try {
			maxNumber = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return maxNumber;
	}

	public EONotifcationPreferences getDisplayNotificationCheckbox() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("notif_checkbox");
		try {
			notificationObject = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return notificationObject;
	}

	public Boolean getNotification() {

		if (notificationObject.stringContent().equalsIgnoreCase("O")) {
			notification = true;
		} else {
			notification = false;
		}
		return notification;
	}

	public void setNotification(Boolean notification) {

		if (notification) {
			notificationObject.setStringContent("O");
		} else {
			notificationObject.setStringContent("N");
		}

		notificationObject.setDModification(new NSTimestamp());

		this.notification = notification;
	}

	public void setForbbidenDomains(EONotifcationPreferences forbbidenDomains) {
		this.forbbidenDomains = forbbidenDomains;
	}

	public void setMaxNumber(EONotifcationPreferences maxNumber) {
		this.maxNumber = maxNumber;
	}

	public EONotifcationPreferences getNb_days_lifetime_link() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("nb_days_lifetime_link");

		try {
			nb_days_lifetime_link = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nb_days_lifetime_link;
	}

	public void setNb_days_lifetime_link(EONotifcationPreferences nb_days_lifetime_link) {
		this.nb_days_lifetime_link = nb_days_lifetime_link;
	}

	public EONotifcationPreferences getFrenchExplication() {
		return frenchExplication;
	}

	public void setFrenchExplication(EONotifcationPreferences frenchExplication) {
		this.frenchExplication = frenchExplication;
	}

	public EONotifcationPreferences getEnglishExplication() {
		return englishExplication;
	}

	public void setEnglishExplication(EONotifcationPreferences englishExplication) {
		this.englishExplication = englishExplication;
	}

	public void save() {
		ec.lock();

		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

		finally {
			ec.unlock();
		}
	}

}
