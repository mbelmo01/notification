package fr.univlr.dsi.fwknotifgui.components;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtil {

	/**
	 * Utility method to send simple HTML email
	 * 
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 */
	public static void sendEmail(String from, String toEmail, String subject, String body) {
		
		System.out.println("from : " + from);
		System.out.println("toEmail : " + toEmail);
		System.out.println("subject : " + subject);
		System.out.println("body : " + body);
				
		System.out.println("SimpleEmail Start");

		String smtpHostServer = "bm-edge.univ-lr.fr";
		String smtpHostPort = "587";

		Properties props = System.getProperties();

		props.put("mail.smtp.host", smtpHostServer);
		props.put("mail.smtp.port", smtpHostPort);
		
		props.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getInstance(props, null);

		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress(from, "NoReply-JD"));

			msg.setReplyTo(InternetAddress.parse(from, false));

			msg.setSubject(subject, "UTF-8");

			msg.setText(body, "UTF-8");
			msg.setContent(msg, "text/html");
			
			
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			System.out.println("Message is ready");
			Transport.send(msg);

			System.out.println("EMail Sent Successfully!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}