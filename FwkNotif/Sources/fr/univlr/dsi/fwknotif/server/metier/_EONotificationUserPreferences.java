// DO NOT EDIT.  Make changes to EONotificationUserPreferences.java instead.
package fr.univlr.dsi.fwknotif.server.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import java.math.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.cocktail.dt.server.metier.A_FwkDTRecord;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONotificationUserPreferences extends  A_FwkDTRecord {
  public static final String ENTITY_NAME = "NotificationUserPref";
	public static final String ENTITY_TABLE_NAME = "HISTOTOX.NOTIFICATION_USER_PREF";

  // Attribute Keys
  public static final ERXKey<String> USER_PREF_NOTIF_ENABLE = new ERXKey<String>("userPrefNotifEnable");
  public static final ERXKey<Integer> USER_PREF_PERS_ID = new ERXKey<Integer>("userPrefPersId");
  // Relationship Keys

  // Attributes
  public static final String USER_PREF_NOTIF_ENABLE_KEY = USER_PREF_NOTIF_ENABLE.key();
  public static final String USER_PREF_PERS_ID_KEY = USER_PREF_PERS_ID.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONotificationUserPreferences.class);

  public EONotificationUserPreferences localInstanceIn(EOEditingContext editingContext) {
    EONotificationUserPreferences localInstance = (EONotificationUserPreferences)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String userPrefNotifEnable() {
    return (String) storedValueForKey(_EONotificationUserPreferences.USER_PREF_NOTIF_ENABLE_KEY);
  }

  public void setUserPrefNotifEnable(String value) {
    if (_EONotificationUserPreferences.LOG.isDebugEnabled()) {
    	_EONotificationUserPreferences.LOG.debug( "updating userPrefNotifEnable from " + userPrefNotifEnable() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationUserPreferences.USER_PREF_NOTIF_ENABLE_KEY);
  }

  public Integer userPrefPersId() {
    return (Integer) storedValueForKey(_EONotificationUserPreferences.USER_PREF_PERS_ID_KEY);
  }

  public void setUserPrefPersId(Integer value) {
    if (_EONotificationUserPreferences.LOG.isDebugEnabled()) {
    	_EONotificationUserPreferences.LOG.debug( "updating userPrefPersId from " + userPrefPersId() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotificationUserPreferences.USER_PREF_PERS_ID_KEY);
  }


  public static EONotificationUserPreferences createNotificationUserPref(EOEditingContext editingContext, String userPrefNotifEnable
, Integer userPrefPersId
) {
    EONotificationUserPreferences eo = (EONotificationUserPreferences) EOUtilities.createAndInsertInstance(editingContext, _EONotificationUserPreferences.ENTITY_NAME);    
		eo.setUserPrefNotifEnable(userPrefNotifEnable);
		eo.setUserPrefPersId(userPrefPersId);
    return eo;
  }

  public static ERXFetchSpecification<EONotificationUserPreferences> fetchSpec() {
    return new ERXFetchSpecification<EONotificationUserPreferences>(_EONotificationUserPreferences.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONotificationUserPreferences> fetchAllNotificationUserPrefs(EOEditingContext editingContext) {
    return _EONotificationUserPreferences.fetchAllNotificationUserPrefs(editingContext, null);
  }

  public static NSArray<EONotificationUserPreferences> fetchAllNotificationUserPrefs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONotificationUserPreferences.fetchNotificationUserPrefs(editingContext, null, sortOrderings);
  }

  public static NSArray<EONotificationUserPreferences> fetchNotificationUserPrefs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONotificationUserPreferences> fetchSpec = new ERXFetchSpecification<EONotificationUserPreferences>(_EONotificationUserPreferences.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONotificationUserPreferences> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONotificationUserPreferences fetchNotificationUserPref(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotificationUserPreferences.fetchNotificationUserPref(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotificationUserPreferences fetchNotificationUserPref(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONotificationUserPreferences> eoObjects = _EONotificationUserPreferences.fetchNotificationUserPrefs(editingContext, qualifier, null);
    EONotificationUserPreferences eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NotificationUserPref that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotificationUserPreferences fetchRequiredNotificationUserPref(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotificationUserPreferences.fetchRequiredNotificationUserPref(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotificationUserPreferences fetchRequiredNotificationUserPref(EOEditingContext editingContext, EOQualifier qualifier) {
    EONotificationUserPreferences eoObject = _EONotificationUserPreferences.fetchNotificationUserPref(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NotificationUserPref that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotificationUserPreferences localInstanceIn(EOEditingContext editingContext, EONotificationUserPreferences eo) {
    EONotificationUserPreferences localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }


//Attributs non visibles
public static final String USER_PREF_ORDRE_KEY = "userPrefOrdre";
public static final ERXKey<Integer> USER_PREF_ORDRE = new ERXKey<Integer>("userPrefOrdre");

/**
 * Créer une instance de EONotificationUserPreferences avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
//  public static  EONotificationUserPreferences createEONotificationUserPreferences(EOEditingContext editingContext//////////, String userPrefNotifEnable
////////////////, Integer userPrefPersId
//////////			) {
//    EONotificationUserPreferences eo = (EONotificationUserPreferences) createAndInsertInstance(editingContext, _EONotificationUserPreferences.ENTITY_NAME);//    
////////////		eo.setUserPrefNotifEnable(userPrefNotifEnable);
////////////////		eo.setUserPrefPersId(userPrefPersId);
//////////    return eo;
//  }


}
