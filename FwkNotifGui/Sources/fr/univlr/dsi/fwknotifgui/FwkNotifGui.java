package fr.univlr.dsi.fwknotifgui;

import er.extensions.ERXFrameworkPrincipal;

public class FwkNotifGui extends ERXFrameworkPrincipal {
	protected static FwkNotifGui sharedInstance;
	@SuppressWarnings("unchecked")
	public final static Class<? extends ERXFrameworkPrincipal> REQUIRES[] = new Class[] {};

	static {
		setUpFrameworkPrincipalClass(FwkNotifGui.class);
	}

	public static FwkNotifGui sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = sharedInstance(FwkNotifGui.class);
		}
		return sharedInstance;
	}

	@Override
	public void finishInitialization() {
		log.debug("FwkNotifGui loaded");
	}
}
