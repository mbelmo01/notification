package fr.univlr.dsi.notif;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlLogin;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORequest;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import fr.univlr.dsi.fwknotif.NotifUserInfo;
import fr.univlr.dsi.fwknotifgui.components.MailValidationPage;
import fr.univlr.dsi.notif.components.NotifWOComponentCtrl;

/**
 * Implemente les actions permettant d'acceder directement au detail à la page
 * de validation d'une adresse mail Notification
 * 
 * @author mbelmo01
 */
public class DAMailValidation extends DirectAction {

	public final static String uuid_key = "uuid";
	private NSDictionary actionParams;

	public DAMailValidation(WORequest request) {
		super(request);
	}

	protected WOActionResults validation(String uuid, String sessionId) {

		System.out.println("Entreee dans validation(" + uuid + ", " + sessionId + ")");

		NSMutableDictionary<String, Object> actionParamsMutable = new NSMutableDictionary<String, Object>();
		actionParamsMutable.setObjectForKey(uuid, "uuid");
		if (sessionId != null) {
			actionParamsMutable.setObjectForKey(sessionId, "wosid");
		}
		actionParams = actionParamsMutable.immutableClone();
		if (useCasService()) {
			// si wosid est précisé, on va réutiliser la session existante
			boolean keepSession = false;
			if (!StringCtrl.isEmpty(sessionId)) {
				keepSession = true;
			}
			return pageForURL(getLoginActionURL(null, false, getClass().getName(), keepSession, actionParams));
		} else {
			return loginNoCasPage(actionParams);
		}
	}

	@Override
	public WOActionResults defaultAction() {

		System.out.println("Entree dans defaultAction() de DAMailValidation");
		String uuid = request().stringFormValueForKey(uuid_key);
		String sessionId = request().stringFormValueForKey("wosid");

		System.out.println("uuid : " + uuid);
		System.out.println("sessionId : " + sessionId);
		return validation(uuid, sessionId);
	}

	@Override
	public String casLoginLink() {
		return getLoginActionURL(null, false, getClass().getName(), false, actionParams);
	}

	@Override
	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		CktlLog.trace(null);

		String errorMsg = notifSession().setConnectedUser(netid);
		if (errorMsg == null) {

			NotifUserInfo appUser = new NotifUserInfo(notifSession().defaultEditingContext(),
					Integer.valueOf(notifSession().connectedUserInfo().persId().intValue()));

			notifSession().setApplicationUser(appUser);
			CktlLog.log("login : " + netid + ", type : validation - OK");
			WOComponent nextPage = getMailValidationPage(notifSession(), actionParams);
			return NotifWOComponentCtrl.redirectWithoutCasCallbackUrl(nextPage);
		}
		return loginCasFailurePage(errorMsg, null);
	}

	private final static WOComponent getMailValidationPage(Session session, NSDictionary actionParams) {
		String uuid = actionParams.valueForKey(uuid_key).toString();

		System.out.println("L'uuid dans getMailValidationPage() : " + uuid);
		MailValidationPage nextPage = (MailValidationPage) session
				.getSavedPageWithName(MailValidationPage.class.getName());

		nextPage.getCtrl().setUuid(uuid);
		return nextPage;
	}

	public class DefaultLoginResponder implements CktlLoginResponder {
		private NSDictionary actionParams;

		public DefaultLoginResponder(NSDictionary actionParams) {
			this.actionParams = actionParams;
		}

		public NSDictionary actionParams() {
			return actionParams;
		}

		public WOComponent loginAccepted(CktlLogin loginComponent) {
			WOComponent nextPage = null;

			Session session = (Session) loginComponent.session();
			CktlLog.trace("session ID : " + session.sessionID());
			CktlLog.trace("" + loginComponent.loggedUserInfo());

			try {
				nextPage = (WOComponent) loginCASPage();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return nextPage;
		}

		public boolean acceptLoginName(String loginName) {
			return cktlApp.acceptLoginName(loginName);
		}

		public boolean acceptEmptyPassword() {
			return cktlApp.config().booleanForKey("ACCEPT_EMPTY_PASSWORD");
		}

		public String getRootPassword() {
			return cktlApp.getRootPassword();
		}
	}

}
