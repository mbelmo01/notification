package fr.univlr.dsi.fwknotifgui.ctrl;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.foundation.ERXProperties;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;

public class FooterCtrl extends ANotifComponentCtrl {
	private String dbName = "";

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public FooterCtrl(ANotifComponent component) {
		super(component);
		String dbProperties = null;
		dbProperties = ERXProperties.stringForKey("SAUT_ID_TRANSLATION");

		if (dbProperties != null) {
			if (dbProperties.equalsIgnoreCase("GRHUM:GRHUMT,ULOGIN:GRHUMT")) {
				dbName = "GestTest";
			} else {
				if (dbProperties.equalsIgnoreCase("GRHUM:GRHUM,ULOGIN:GRHUM")) {
					dbName = "Gest";
				} else {
					dbName = "Autre";
				}
			}
		} else {
			dbName = "Aucune BDD renseignée";
		}
	}

	EOEditingContext ec = session().defaultEditingContext();
	private EOIndividu connectedIndividu = EOIndividu.individuWithPersId(ec, session().applicationUser().getPersId());

	public String getLogin() {
		String noIndividuLogin = connectedIndividu.noIndividuLogin();
		String[] strings = noIndividuLogin.split("/");
		return strings[1];
	}

	public Boolean isAdmin() {
		
		
		Boolean rep = false;

//		if (isAdmin(connectedIndividu.persId())) {
//			rep = true;
//		}
		
		rep = session().isAdmin();

		return rep;
	}

	
//	public boolean isAdmin(Integer persId) {
//
//		Boolean rep = false;
//
//		rep = this.isMembre(C_STRUCTURE_ADMIN_KEY, persId);
//		
//		return rep;
//	}
//	public final static String C_STRUCTURE_ADMIN_KEY = "fr.univlr.dsi.notif.droit.cstructrureadmin";
//
//	private final boolean isMembre(String cStructureConfigKey, Integer persId) {
//
//		boolean isMembre = false;
//
//		NSArray<String> cStructures = ERXProperties.arrayForKey(C_STRUCTURE_ADMIN_KEY);
//
//		for (String cStructure : cStructures) {
//			if (EOStructureForGroupeSpec.isPersonneInGroupe(EOIndividu.individuWithPersId(ec, persId),
//					EOStructure.structurePourCode(ec, cStructure))) {
//				isMembre = true;
//			}
//		}
//
//		return isMembre;
//	}
}
