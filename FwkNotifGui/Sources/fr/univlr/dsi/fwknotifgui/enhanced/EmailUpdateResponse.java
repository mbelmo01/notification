package fr.univlr.dsi.fwknotifgui.enhanced;

public class EmailUpdateResponse {

	Boolean isValidForm = false;
	Boolean isValidDomain = false;
	Boolean error = false;
	String errorMessage = "";
	
	public Boolean displayRegisterButton = false;
	public Boolean displayConfirmButton = false;
	public Boolean displayUpdateButton = false;
	public Boolean displayAdresseEnCoursDeValidation = false;
	public Boolean displayNotification = false;
	
	
	public void resetAllFlags(){
		
	}
}
