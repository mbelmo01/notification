package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSMutableArray;

import fr.univlr.dsi.fwknotif.server.metier.EONotifcationPreferences;

public class PreferencesUtils {

	EOEditingContext ec = null;

	EONotifcationPreferences explication = null;
	private NSMutableArray<String> domainesInterdits = new NSMutableArray<String>();

	public PreferencesUtils(EOEditingContext ec) {
		this.ec = ec;
	}

	public EONotifcationPreferences getExplication(String lang) {

		EOQualifier qual1 = null;

		if (lang == null) {
			lang = "fr";
		}

		if (lang.equalsIgnoreCase("fr")) {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("explication_fr");
		} else {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("explication_en");
		}

		try {
			explication = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return explication;
	}

	public EONotifcationPreferences getNb_days_lifetime_link() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("nb_days_lifetime_link");

		EONotifcationPreferences nb_days_lifetime_link = null;
		try {
			nb_days_lifetime_link = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nb_days_lifetime_link;
	}

	public EONotifcationPreferences getFrom() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("mail_from");

		try {

			return EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public EONotifcationPreferences getCc() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("mail_cc");

		try {

			return EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public EONotifcationPreferences getSubject() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("mail_subject");

		try {

			return EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public EONotifcationPreferences getMail() {

		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("mail");

		try {
			return EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public NSMutableArray<String> getDomainesInterdits() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("domains");
		EONotifcationPreferences domains = null;
		domainesInterdits = new NSMutableArray<String>(); 
		try {
			domains = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (domains != null) {
			String[] res = domains.stringContent().split(",");

			for (String r : res) {
				r.trim();
				domainesInterdits.add(r);
			}
		}

		return domainesInterdits;
	}

	public EONotifcationPreferences getDomainesInterditsString() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("domains");
		EONotifcationPreferences domains = null;

		try {
			domains = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();

		}

		return domains;

	}

	public EONotifcationPreferences getNotificationCheckBox() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("notif_checkbox");
		EONotifcationPreferences notificationObject = null;

		try {
			notificationObject = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return notificationObject;
	}

	public Integer getMaxNumber(EOEditingContext ec) {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("max_phones");
		EONotifcationPreferences max = null;
		try {
			max = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Integer.parseInt(max.stringContent());

	}

	public Boolean displayNotificationCheckbox() {
		EOQualifier qual1 = EONotifcationPreferences.STRING_TYPE.eq("notif_checkbox");
		EONotifcationPreferences notificationObject = null;

		try {
			notificationObject = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Boolean rep = false;

		if (notificationObject != null) {
			if (notificationObject.stringContent().equalsIgnoreCase("O")) {
				rep = true;
			}
		}

		return rep;
	}

	public EONotifcationPreferences getTitle(String lang) {

		EONotifcationPreferences title = null;
		EOQualifier qual1 = null;

		if (lang.equalsIgnoreCase("fr")) {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("title_fr");
		} else {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("title_en");
		}

		try {
			title = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();

		}

		return title;

	}
	
	public EONotifcationPreferences getSubTitle(String lang) {

		EONotifcationPreferences title = null;
		EOQualifier qual1 = null;

		if (lang.equalsIgnoreCase("fr")) {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("subtitle_fr");
		} else {
			qual1 = EONotifcationPreferences.STRING_TYPE.eq("subtitle_en");
		}

		try {
			title = EONotifcationPreferences.fetchNotifcationPreferences(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();

		}
		
		return title;

	}
}
