package fr.univlr.dsi.fwknotifgui.ctrl;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;

import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;

public class WelcomePageCtrl extends ANotifComponentCtrl {

	public WelcomePageCtrl(ANotifComponent component) {
		super(component);

		// connectedIndividu = EOIndividu.individuWithPersId(ec, new
		// Integer(session().connectedUserInfo().persId().intValue()));
		connectedIndividu = EOIndividu.individuWithPersId(ec, session().applicationUser().getPersId());
	}

	EOEditingContext ec = session().defaultEditingContext();
	// private EOIndividu connectedIndividu = EOIndividu.individuWithPersId(ec,
	// session().applicationUser().getPersId());
	private EOIndividu connectedIndividu = null;

	String mail = "";

	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}

	public EOIndividu getConnectedIndividu() {
		return connectedIndividu;
	}

	public void setConnectedIndividu(EOIndividu connectedIndividu) {
		this.connectedIndividu = connectedIndividu;
	}

	public String getMail() {
		if (connectedIndividu != null) {
			mail = connectedIndividu.getEmails(null).get(0);
		}

		return mail;
	}

	public void setMail(String mail) {
		if (connectedIndividu != null) {
			mail = connectedIndividu.getEmails(null).get(0);
		}
		this.mail = mail;
	}

	public Boolean isAdmin() {
		return session().isAdmin();
	}

	public Boolean isRoot() {
		return session().isRoot();
	}
}
