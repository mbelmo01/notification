package fr.univlr.dsi.notif;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORedirect;

import er.extensions.localization.ERXLocalizer;
import fr.univlr.dsi.fwknotif.INotifSession;
import fr.univlr.dsi.fwknotif.NotifUserInfo;
import fr.univlr.dsi.fwknotifgui.components.MailValidationPage;

public class Session extends CocktailAjaxSession implements INotifSession {
	private static final long serialVersionUID = 1L;
	private NotifUserInfo applicationUser;
	private WOComponent _pageContenu;

	public Session() {
		super();
	}

	@Override
	public Application application() {
		return (Application) super.application();
	}

	public NotifUserInfo applicationUser() {
		return applicationUser;
	}

	public void setApplicationUser(NotifUserInfo appUser) {
		this.applicationUser = appUser;
	}

	public WOComponent pageContenu() {
		return _pageContenu;
	}

	public void setPageContenu(WOComponent value) {
		_pageContenu = value;
	}

	public Boolean isAdmin() {
		// return applicationUser.isAdmin(session().connectedUserInfo());
		return applicationUser.isAdmin(applicationUser.getPersId());

		// applicationUser.getPersId();
		// return true;
	}

	public Boolean isRoot() {
		return applicationUser.isRoot(applicationUser.getPersId());
	}

	@Override
	public ERXLocalizer localizer() {
		return super.localizer();
	}

	public Application notifApp() {
		return (Application) WOApplication.application();
	}

	// public CktlWebSession getCktlWebSession(){
	// }

	/*
	 * 
	 * ajout pour permettre la validation par mail
	 */

	public WOComponent selectValidation(String uuid) {

		MailValidationPage page = (MailValidationPage) getSavedPageWithName(MailValidationPage.class.getName());

		if (uuid != null) {

			page.getCtrl().setUuid(uuid);

			setPageContenu(page);

		}
		return pageContenu();
	}

	/*
	 * nouvel ajout pour permettre l'authentification lors de la validation par
	 * mail
	 */

	protected WOComponent pageForURL(String url) {

		if (url == null) {
			return null;
		}

		WORedirect page = (WORedirect) cktlApp.pageWithName("WORedirect", context());
		page.setUrl(url);

		
		return page;
	}

	public String appMailValidationURL(String uuid, boolean isAjouterSessionId) {
		String url = "";

		url = notifApp().getApplicationURL(context()) + "/wa/DAMailValidation?uuid=" + uuid;

		System.out.println("ULR -------> " + url);
		if (isAjouterSessionId) {
			url += "&wosid=" + sessionID();
		}

		return url;
	}
}
