// DO NOT EDIT.  Make changes to NEOPersonneTelephone.java instead.
package fr.univlr.dsi.fwknotif.server.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import java.math.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.cocktail.dt.server.metier.A_FwkDTRecord;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _NEOPersonneTelephone extends  A_FwkDTRecord {
  public static final String ENTITY_NAME = "NPersonneTelephone";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PERSONNE_TELEPHONE";

  // Attribute Keys
  public static final ERXKey<String> C_STRUCTURE = new ERXKey<String>("cStructure");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_DEB_VAL = new ERXKey<NSTimestamp>("dDebVal");
  public static final ERXKey<NSTimestamp> D_FIN_VAL = new ERXKey<NSTimestamp>("dFinVal");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> INDICATIF = new ERXKey<Integer>("indicatif");
  public static final ERXKey<String> LISTE_ROUGE = new ERXKey<String>("listeRouge");
  public static final ERXKey<String> NO_TELEPHONE = new ERXKey<String>("noTelephone");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<String> TEL_PRINCIPAL = new ERXKey<String>("telPrincipal");
  public static final ERXKey<String> TYPE_NO = new ERXKey<String>("typeNo");
  public static final ERXKey<String> TYPE_TEL = new ERXKey<String>("typeTel");
  // Relationship Keys

  // Attributes
  public static final String C_STRUCTURE_KEY = C_STRUCTURE.key();
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_DEB_VAL_KEY = D_DEB_VAL.key();
  public static final String D_FIN_VAL_KEY = D_FIN_VAL.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String INDICATIF_KEY = INDICATIF.key();
  public static final String LISTE_ROUGE_KEY = LISTE_ROUGE.key();
  public static final String NO_TELEPHONE_KEY = NO_TELEPHONE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String TEL_PRINCIPAL_KEY = TEL_PRINCIPAL.key();
  public static final String TYPE_NO_KEY = TYPE_NO.key();
  public static final String TYPE_TEL_KEY = TYPE_TEL.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_NEOPersonneTelephone.class);

  public NEOPersonneTelephone localInstanceIn(EOEditingContext editingContext) {
    NEOPersonneTelephone localInstance = (NEOPersonneTelephone)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cStructure() {
    return (String) storedValueForKey(_NEOPersonneTelephone.C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating cStructure from " + cStructure() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.C_STRUCTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(_NEOPersonneTelephone.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(_NEOPersonneTelephone.D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(_NEOPersonneTelephone.D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_NEOPersonneTelephone.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.D_MODIFICATION_KEY);
  }

  public Integer indicatif() {
    return (Integer) storedValueForKey(_NEOPersonneTelephone.INDICATIF_KEY);
  }

  public void setIndicatif(Integer value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating indicatif from " + indicatif() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.INDICATIF_KEY);
  }

  public String listeRouge() {
    return (String) storedValueForKey(_NEOPersonneTelephone.LISTE_ROUGE_KEY);
  }

  public void setListeRouge(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating listeRouge from " + listeRouge() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.LISTE_ROUGE_KEY);
  }

  public String noTelephone() {
    return (String) storedValueForKey(_NEOPersonneTelephone.NO_TELEPHONE_KEY);
  }

  public void setNoTelephone(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating noTelephone from " + noTelephone() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.NO_TELEPHONE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(_NEOPersonneTelephone.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.PERS_ID_KEY);
  }

  public String telPrincipal() {
    return (String) storedValueForKey(_NEOPersonneTelephone.TEL_PRINCIPAL_KEY);
  }

  public void setTelPrincipal(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating telPrincipal from " + telPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.TEL_PRINCIPAL_KEY);
  }

  public String typeNo() {
    return (String) storedValueForKey(_NEOPersonneTelephone.TYPE_NO_KEY);
  }

  public void setTypeNo(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating typeNo from " + typeNo() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.TYPE_NO_KEY);
  }

  public String typeTel() {
    return (String) storedValueForKey(_NEOPersonneTelephone.TYPE_TEL_KEY);
  }

  public void setTypeTel(String value) {
    if (_NEOPersonneTelephone.LOG.isDebugEnabled()) {
    	_NEOPersonneTelephone.LOG.debug( "updating typeTel from " + typeTel() + " to " + value);
    }
    takeStoredValueForKey(value, _NEOPersonneTelephone.TYPE_TEL_KEY);
  }


  public static NEOPersonneTelephone createNPersonneTelephone(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String noTelephone
, Integer persId
, String telPrincipal
, String typeNo
, String typeTel
) {
    NEOPersonneTelephone eo = (NEOPersonneTelephone) EOUtilities.createAndInsertInstance(editingContext, _NEOPersonneTelephone.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoTelephone(noTelephone);
		eo.setPersId(persId);
		eo.setTelPrincipal(telPrincipal);
		eo.setTypeNo(typeNo);
		eo.setTypeTel(typeTel);
    return eo;
  }

  public static ERXFetchSpecification<NEOPersonneTelephone> fetchSpec() {
    return new ERXFetchSpecification<NEOPersonneTelephone>(_NEOPersonneTelephone.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<NEOPersonneTelephone> fetchAllNPersonneTelephones(EOEditingContext editingContext) {
    return _NEOPersonneTelephone.fetchAllNPersonneTelephones(editingContext, null);
  }

  public static NSArray<NEOPersonneTelephone> fetchAllNPersonneTelephones(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _NEOPersonneTelephone.fetchNPersonneTelephones(editingContext, null, sortOrderings);
  }

  public static NSArray<NEOPersonneTelephone> fetchNPersonneTelephones(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<NEOPersonneTelephone> fetchSpec = new ERXFetchSpecification<NEOPersonneTelephone>(_NEOPersonneTelephone.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<NEOPersonneTelephone> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static NEOPersonneTelephone fetchNPersonneTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _NEOPersonneTelephone.fetchNPersonneTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static NEOPersonneTelephone fetchNPersonneTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<NEOPersonneTelephone> eoObjects = _NEOPersonneTelephone.fetchNPersonneTelephones(editingContext, qualifier, null);
    NEOPersonneTelephone eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NPersonneTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static NEOPersonneTelephone fetchRequiredNPersonneTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _NEOPersonneTelephone.fetchRequiredNPersonneTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static NEOPersonneTelephone fetchRequiredNPersonneTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    NEOPersonneTelephone eoObject = _NEOPersonneTelephone.fetchNPersonneTelephone(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NPersonneTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static NEOPersonneTelephone localInstanceIn(EOEditingContext editingContext, NEOPersonneTelephone eo) {
    NEOPersonneTelephone localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }


//Attributs non visibles
public static final String TELEPHONE_ID_KEY = "telephoneId";
public static final ERXKey<Integer> TELEPHONE_ID = new ERXKey<Integer>("telephoneId");

/**
 * Créer une instance de NEOPersonneTelephone avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
//  public static  NEOPersonneTelephone createNEOPersonneTelephone(EOEditingContext editingContext//////////////, NSTimestamp dCreation
////////////////////////, NSTimestamp dModification
////////////////////////, String noTelephone
////////////////, Integer persId
////////////////, String telPrincipal
////////////////, String typeNo
////////////////, String typeTel
//////////			) {
//    NEOPersonneTelephone eo = (NEOPersonneTelephone) createAndInsertInstance(editingContext, _NEOPersonneTelephone.ENTITY_NAME);//    
////////////////		eo.setDCreation(dCreation);
////////////////////////		eo.setDModification(dModification);
////////////////////////		eo.setNoTelephone(noTelephone);
////////////////		eo.setPersId(persId);
////////////////		eo.setTelPrincipal(telPrincipal);
////////////////		eo.setTypeNo(typeNo);
////////////////		eo.setTypeTel(typeTel);
//////////    return eo;
//  }


}
