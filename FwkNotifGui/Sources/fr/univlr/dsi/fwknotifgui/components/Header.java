package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.HeaderCtrl;

public class Header extends ANotifComponent {

	private final static String BND_ctrl = "ctrl";

	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	public Class<?> getClassCtrl() {
		return HeaderCtrl.class;
	}

	public HeaderCtrl getCtrl() {
		return (HeaderCtrl) super.getCtrl();
	}

	public Header(WOContext context) {
		super(context);
	}

	public WOActionResults goToWelcomePage() {
		return pageWithName(WelcomePage.class.getName());
	}

}