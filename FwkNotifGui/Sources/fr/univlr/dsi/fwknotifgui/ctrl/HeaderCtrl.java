package fr.univlr.dsi.fwknotifgui.ctrl;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.foundation.ERXProperties;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;

public class HeaderCtrl extends ANotifComponentCtrl {
	String displayHomeButton;

	public HeaderCtrl(ANotifComponent component) {
		super(component);
		displayHomeButton = ERXProperties.stringForKey("fr.univlr.dsi.notif.display.home.button");
	}

	EOEditingContext ec = session().defaultEditingContext();
	private EOIndividu connectedIndividu = EOIndividu.individuWithPersId(ec, session().applicationUser().getPersId());

	public String getLogin() {
		String noIndividuLogin = connectedIndividu.noIndividuLogin();
		String[] strings = noIndividuLogin.split("/");
		return strings[1];
	}
}
