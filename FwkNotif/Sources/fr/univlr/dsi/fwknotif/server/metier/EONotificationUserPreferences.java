package fr.univlr.dsi.fwknotif.server.metier;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSValidation;

public class EONotificationUserPreferences extends _EONotificationUserPreferences {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EONotificationUserPreferences.class);

	public EONotificationUserPreferences() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    
    public EONotificationUserPreferences getUserNotificationPreference(EOEditingContext ec, Integer persId){
    	EONotificationUserPreferences n = null;

		EOQualifier qual1 = EONotificationUserPreferences.USER_PREF_PERS_ID.eq(persId);
		
		try {
			n = EONotificationUserPreferences.fetchNotificationUserPref(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return n;
    }
    
    
    public static Boolean getUserNotificationPreferenceValue(EOEditingContext ec, Integer persId){
    	EONotificationUserPreferences n = null;
    	Boolean rep = null;
    	
		EOQualifier qual1 = EONotificationUserPreferences.USER_PREF_PERS_ID.eq(persId);
		
		try {
			n = EONotificationUserPreferences.fetchNotificationUserPref(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (n != null) {
			if (n.userPrefNotifEnable().equalsIgnoreCase("O")) {
				rep = true;
			} else {
				rep = false;
			}
		} else {
			rep = false;
		}
		return rep;
    }
    
    
    
    public static void setUserNotificationPreference(EOEditingContext ec, Integer persId, Boolean value){
    	EONotificationUserPreferences n = null;

		EOQualifier qual1 = EONotificationUserPreferences.USER_PREF_PERS_ID.eq(persId);

		try {
			n = EONotificationUserPreferences.fetchNotificationUserPref(ec, qual1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (n != null) {

			if (value == true) {
				n.setUserPrefNotifEnable("O");
			} else {
				n.setUserPrefNotifEnable("N");
			}
		} else {

			try {
				String enable = "";

				if (value == true) {
					enable = "O";

				} else {
					enable = "N";

				}

				n = EONotificationUserPreferences.createNotificationUserPref(ec, enable, persId);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			save(ec);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    
    public static void save(EOEditingContext ec) {
		ec.lock();

		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

		finally {
			ec.unlock();
		}
	}

}
