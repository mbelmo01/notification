package fr.univlr.dsi.fwknotif;

import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.localization.ERXLocalizer;

public interface INotifSession {
	
	EOEditingContext defaultEditingContext();

	NotifUserInfo applicationUser();

	public Boolean isAdmin();
	
	public Boolean isRoot();
	
	CktlMailBus mailBus();
	
	WOComponent logout();
	
	WOContext context();
	
	ERXLocalizer localizer();
	
	CktlUserInfo connectedUserInfo();

//	CktlWebSession cktlWebSession();
	
}
