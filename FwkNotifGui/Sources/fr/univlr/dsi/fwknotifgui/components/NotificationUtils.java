package fr.univlr.dsi.fwknotifgui.components;

import java.util.Comparator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.INotifSession;
import fr.univlr.dsi.fwknotif.server.metier.NEOPersonneTelephone;

public class NotificationUtils {

	private final static Logger log = Logger.getLogger(NotificationUtils.class);

	public static NSArray<EOPaysIndicatif> sortByIndicatif(NSArray<EOPaysIndicatif> liste) {
		liste.sort(Comparator.comparing(EOPaysIndicatif::indicatif));

		EOPaysIndicatif fr = null;

		for (EOPaysIndicatif i : liste) {
			if (i.indicatif() == 33) {
				fr = i;
			}
		}

		liste.remove(fr);

		liste.add(0, fr);
		return liste;
	}

	public static boolean isValidEmailFormat(String email) {
		Boolean rep = false;
		EmailValidatorUlr ev = new EmailValidatorUlr();
		try {
			if (email != null) {
				rep = EmailValidatorUlr.validate(email);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (rep) {
			log.info("Cette adresse est valide : " + email);
		} else {
			log.info("Cette adresse n'est pas valide : " + email);
		}

		return rep;
	}

	public static NSMutableArray<EORepartPersonneAdresseEnhanced> fillAdresses(EOEditingContext ec,	EOIndividu connectedIndividu, INotifSession session, INotifApplication app) {

		NSMutableArray<EORepartPersonneAdresseEnhanced> adresses = new NSMutableArray<EORepartPersonneAdresseEnhanced>();
		EOQualifier qual1 = EORepartPersonneAdresse.PERS_ID.eq(connectedIndividu.persId());
		EOQualifier qual2 = EORepartPersonneAdresse.RPA_VALIDE.eq("O");
		EOQualifier qual3 = null;

		if (connectedIndividu.isEtudiant()) {
			qual3 = EORepartPersonneAdresse.TADR_CODE.eq("ETUD");
		}

		if (connectedIndividu.isPersonnel()) {
			qual3 = EORepartPersonneAdresse.TADR_CODE.eq("PERSO");
		}

		if ((connectedIndividu.isEtudiant() && connectedIndividu.isPersonnel())
				|| ((!connectedIndividu.isEtudiant()) && (!connectedIndividu.isPersonnel()))) {
			NSMutableArray<String> codes = new NSMutableArray<String>();
			codes.add("ETUD");
			codes.add("PERSO");
			qual3 = EORepartPersonneAdresse.TADR_CODE.in(codes);
		}

		EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);

		NSArray<EORepartPersonneAdresse> eoAdresses = EORepartPersonneAdresse.fetchAll(ec, qualifierFinale);

		for (EORepartPersonneAdresse eoAdd : eoAdresses) {
			adresses.add(new EORepartPersonneAdresseEnhanced(eoAdd, session, app));
		}

		return adresses;
	}

//	public static NSArray<EOPersonneTelephoneEnhanced> fillTelephones(EOEditingContext ec, EOIndividu individu) {
//
//		NSMutableArray<EOPersonneTelephoneEnhanced> telephones = new NSMutableArray<EOPersonneTelephoneEnhanced>();
//		NSArray<EOPaysIndicatif> paysIndicatifs = null;
//		EOPaysIndicatif paysIndicatif = null;
//		EOPaysIndicatif selectedPaysIndicatif = null;
//		paysIndicatifs = EOPaysIndicatif.fetchAll(ec);
//
//		NSMutableArray<EOTypeTel> eoTypeTels = getEOTypeTelSelect(ec, individu);
//		EOTypeTel eoTypeTel = null;
//
//		if (individu != null) {
//
//			EOQualifier qual1 = EOPersonneTelephone.PERS_ID.eq(individu.persId());
//			EOQualifier qual2 = EOPersonneTelephone.TYPE_NO.eq("MOB");
//			EOQualifier qual3 = null;
//
//			if (individu.isEtudiant()) {
//				log.info(individu.getNomAndPrenom() + " isEtudiant() : true");
//				qual3 = EOPersonneTelephone.TYPE_TEL.eq("ETUD");
//
//			}
//
//			if (individu.isPersonnel()) {
//				log.info(individu.getNomAndPrenom() + " isPersonnel() : true");
//				NSMutableArray<String> codes = new NSMutableArray<String>();
//				codes.add("PRV");
//				codes.add("PRF");
//				qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
//			}
//
//			if ((individu.isEtudiant() && individu.isPersonnel())
//					|| ((!individu.isEtudiant()) && (!individu.isPersonnel()))) {
//				NSMutableArray<String> codes = new NSMutableArray<String>();
//				codes.add("ETUD");
//				codes.add("PRF");
//				codes.add("PRV");
//				qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
//			}
//
//			EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);
//
//			NSArray<NEOPersonneTelephone> eoPersonneTelephones = null;
//			try {
//				eoPersonneTelephones = NEOPersonneTelephone.fetchNPersonneTelephones(ec, qualifierFinale, null);
//				log.info("il a " + eoPersonneTelephones.count() + " numeros");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			if (eoPersonneTelephones != null) {
//				for (NEOPersonneTelephone tel : eoPersonneTelephones) {
//					try {
//						telephones.add(new EOPersonneTelephoneEnhanced(ec, tel, paysIndicatifs, paysIndicatif,
//								eoTypeTels, eoTypeTel));
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		}
//
//		return telephones;
//	}

//	public static NSMutableArray<EOTypeTel> getEOTypeTelSelect(EOEditingContext ec, EOIndividu connectedIndividu) {
//		NSMutableArray<EOTypeTel> eoTypeTels = null;
//
//		eoTypeTels = new NSMutableArray<EOTypeTel>();
//		eoTypeTels.clear();
//		if (connectedIndividu.isEtudiant()) {
//			eoTypeTels.add(EOTypeTel.typeEtudiant(ec));
//		} else {
//			if (connectedIndividu.isPersonnel()) {
//				eoTypeTels.add((EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, "PRF"));
//				eoTypeTels.add((EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, "PRV"));
//			} else {
//				eoTypeTels.add((EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, "ETUD"));
//				eoTypeTels.add((EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, "PRF"));
//				eoTypeTels.add((EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, "PRV"));
//			}
//		}
//		return eoTypeTels;
//	}

	public static String cleanPhone(String noTelephone) {

		if (noTelephone != null) {
			try {
				noTelephone = noTelephone.trim();
				noTelephone = noTelephone.replaceAll(" ", "");
				noTelephone = noTelephone.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "+");
				noTelephone = noTelephone.replaceAll("[A-z]", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return noTelephone;
	}

//	public static Boolean isDuplicatePhoneOnAdd(EOEditingContext ec, EOIndividu individu, Integer indicatif,
//			String noTelephone, String typeTel) {
//		Boolean rep = false;
//
//		NSArray<EOPersonneTelephone> tels = new NSArray<EOPersonneTelephone>();
//
//		EOQualifier qual1 = EOPersonneTelephone.PERS_ID.eq(individu.persId());
//		EOQualifier qual2 = EOPersonneTelephone.INDICATIF.eq(indicatif);
//		EOQualifier qual3 = EOPersonneTelephone.NO_TELEPHONE.eq(noTelephone);
//		EOQualifier qual4 = EOPersonneTelephone.TYPE_NO.eq("MOB");
//		EOQualifier qual5 = EOPersonneTelephone.TYPE_TEL.eq(typeTel);
//
//		EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3, qual4, qual5);
//
//		tels = EOPersonneTelephone.fetchAll(ec, qualifierFinale);
//
//		/*
//		 * PERS_ID", "NO_TELEPHONE", "TYPE_NO", "TYPE_TEL", "C_STRUCTURE"
//		 */
//
//		for (EOPersonneTelephone tel : tels) {
//			if (tel.persId().equals(individu.persId()) && tel.noTelephone().equalsIgnoreCase(noTelephone)
//					&& tel.typeTel().equalsIgnoreCase(typeTel)) {
//				rep = true;
//			}
//		}
//		return rep;
//	}

//	public static Boolean isDuplicatePhoneOnUpdate(EOEditingContext ec, EOIndividu individu, Integer indicatif,
//			String noTelephone, String typeTel) {
//		Boolean rep = false;
//
//		NSArray<EOPersonneTelephone> tels = new NSArray<EOPersonneTelephone>();
//
//		EOQualifier qual1 = EOPersonneTelephone.PERS_ID.eq(individu.persId());
//		EOQualifier qual2 = EOPersonneTelephone.INDICATIF.eq(indicatif);
//		EOQualifier qual3 = EOPersonneTelephone.NO_TELEPHONE.eq(noTelephone);
//		EOQualifier qual4 = EOPersonneTelephone.TYPE_NO.eq("MOB");
//		EOQualifier qual5 = EOPersonneTelephone.TYPE_TEL.eq(typeTel);
//
//		EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3, qual4, qual5);
//
//		tels = EOPersonneTelephone.fetchAll(ec, qualifierFinale);
//
//		/*
//		 * PERS_ID", "NO_TELEPHONE", "TYPE_NO", "TYPE_TEL", "C_STRUCTURE"
//		 */
//
//		for (EOPersonneTelephone tel : tels) {
//			if (tel.persId().equals(individu.persId()) && tel.noTelephone().equalsIgnoreCase(noTelephone)
//					&& tel.typeTel().equalsIgnoreCase(typeTel)) {
//				rep = true;
//			}
//		}
//		return rep;
//	}

//	public static Boolean isFrenchPhone(Integer indicatif, String noTelephone) {
//		Boolean rep = false;
//
//		if (indicatif == 33) {
//			if (noTelephone.length() >= 10 && noTelephone.length() <= 13) {
//				rep = true;
//			}
//		}
//
//		return rep;
//	}
//
//	public static Boolean isFrenchCodeCountry(Integer code) {
//		Boolean rep = false;
//
//		if (code == 33) {
//			rep = true;
//		}
//
//		return rep;
//	}
//
//	public static Boolean isFrenchMobile(String noTelephone) {
//		Boolean rep = false;
//		if (noTelephone.length() > 2) {
//			String debut = noTelephone.substring(0, 2);
//
//			if (debut.equalsIgnoreCase("06") || debut.equalsIgnoreCase("07")) {
//				rep = true;
//			}
//		}
//
//		return rep;
//	}
//
//	public static Boolean isValidSize(Integer indicatif, String noTelephone) {
//		Boolean rep = false;
//
//		if (indicatif == 33) {
//			if (noTelephone.length() >= 10 && noTelephone.length() <= 13) {
//				rep = true;
//			}
//		} else {
//			if (noTelephone.length() >= 2 && noTelephone.length() <= 20) {
//				rep = true;
//			}
//		}
//
//		return rep;
//	}
//
//	public static PhoneFormatResponse verifierFormatTelephone(Integer indicatif, String noTelephone) {
//		Boolean frenchCodeCountry = false;
//		Boolean frenchPhone = false;
//		Boolean frenchPhoneMobile = false;
//		Boolean validSize = false;
//		Boolean rep = false;
//
//		PhoneFormatResponse response = new PhoneFormatResponse();
//		if (noTelephone != null) {
//			frenchCodeCountry = isFrenchCodeCountry(indicatif);
//			if (frenchCodeCountry) {
//				frenchPhone = isFrenchPhone(indicatif, noTelephone);
//				frenchPhoneMobile = isFrenchMobile(noTelephone);
//			}
//			validSize = isValidSize(indicatif, noTelephone);
//
//			if (frenchCodeCountry && frenchPhoneMobile && validSize) {
//				rep = true;
//			}
//
//			if ((!frenchCodeCountry) && validSize) {
//				rep = true;
//			}
//
//			response.setFrenchCodeCountry(frenchCodeCountry);
//			response.setFrenchPhone(frenchPhoneMobile);
//			response.setFrenchPhone(frenchPhone);
//			response.setFrenchPhoneMobile(frenchPhoneMobile);
//			response.setValidSize(validSize);
//			response.setRep(rep);
//		}
//
//		return response;
//	}
//
//	public static void createPhoneEntry(EOEditingContext ec, EOIndividu individu, EOPaysIndicatif selectedPaysIndicatif,
//			String noTelephone, EOTypeTel eoTypeTel, CktlMailBus mailBus) throws NotificationException {
//
//		NSTimestamp dCreation = new NSTimestamp();
//		Integer persId = individu.persId();
//		EOTypeNoTel toTypeNoTel = EOTypeNoTel.typePortable(ec);
//		EOTypeTel toTypeTel = null;
//		String typeTel = "";
//		NEOPersonneTelephone phone = null;
//
//		if (individu.isEtudiant()) {
//			typeTel = "ETUD";
//			toTypeTel = EOTypeTel.typeEtudiant(ec);
//
//		} else {
//			if (individu.isPersonnel()) {
//				typeTel = eoTypeTel.cTypeTel();
//				toTypeTel = (EOTypeTel) EOTypeTel.getTypeTelCache().objectForKey(ec, typeTel);
//			} else {
//				if ((individu.isEtudiant() && individu.isPersonnel())
//						|| ((!individu.isEtudiant()) && (!individu.isPersonnel()))) {
//					typeTel = "ETUD";
//					toTypeTel = EOTypeTel.typeEtudiant(ec);
//				}
//			}
//		}
//
//		try {
//			log.info(individu.getNomAndPrenom() + " (" + individu.persId() + ") ajoute un nouveau numéro de téléphone qui est le " + noTelephone);
////			phone = NEOPersonneTelephone.createNPersonneTelephone(ec, dCreation, dCreation, noTelephone, persId, "N", "MOB", typeTel, toTypeNoTel, toTypeTel);
////			phone.setListeRouge("N");
////			phone.setDDebVal(new NSTimestamp());
//
//			if (selectedPaysIndicatif != null) {
//				phone.setIndicatif(selectedPaysIndicatif.indicatif());
//			} else {
//				phone.setIndicatif(33);
//			}
//
//			save(ec);
//		} catch (Exception e) {
//			String error = "Impossible de créer le numéro de téléphone et de l'associer --> " + phone.noTelephone();
//			throw new NotificationException(individu, e, error);
//		}
//	}

	public static Boolean save(EOEditingContext ec) throws NotificationException {
		Boolean rep = false;
		ec.lock();

		try {
			ec.saveChanges();
			rep = true;
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
			rep = false;
		}

		finally {
			ec.unlock();
		}
		
		return rep;
	}
}
