package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.AddressBookCtrl;

public class AddressBook extends ANotifComponent {
	
	private final static String BND_ctrl = "ctrl";

    public AddressBook(WOContext context) {
        super(context);
    }
	
	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	public Class<?> getClassCtrl() {
		return AddressBookCtrl.class;
	}

	public AddressBookCtrl getCtrl() {
		return (AddressBookCtrl) super.getCtrl();
	}
	
	public WOActionResults reload() {
		return this;
	}
	
}