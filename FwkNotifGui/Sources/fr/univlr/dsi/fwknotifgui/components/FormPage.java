package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.FormPageCtrl;

public class FormPage extends ANotifComponent {
	
	private final static String BND_ctrl = "ctrl";
	
    public FormPage(WOContext context) {
        super(context);
    }
    
    public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	public Class<?> getClassCtrl() {
		return FormPageCtrl.class;
	}

	public FormPageCtrl getCtrl() {
		return (FormPageCtrl) super.getCtrl();
	}
	
	public WOActionResults reload() {
		return this;
	}
}