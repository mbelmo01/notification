package fr.univlr.dsi.fwknotif;

import er.extensions.ERXFrameworkPrincipal;

public class FwkNotif extends ERXFrameworkPrincipal {
	protected static FwkNotif sharedInstance;
	@SuppressWarnings("unchecked")
	public final static Class<? extends ERXFrameworkPrincipal> REQUIRES[] = new Class[] {};

	static {
		setUpFrameworkPrincipalClass(FwkNotif.class);
	}

	public static FwkNotif sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = sharedInstance(FwkNotif.class);
		}
		return sharedInstance;
	}

	@Override
	public void finishInitialization() {
		log.debug("FwkNotif loaded");
	}
}
