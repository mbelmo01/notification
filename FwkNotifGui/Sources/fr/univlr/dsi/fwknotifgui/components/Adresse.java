package fr.univlr.dsi.fwknotifgui.components;

import java.util.Date;

import org.apache.commons.codec.binary.Base64;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.foundation.NSTimestamp;

public class Adresse {
	private String typeAdresse = null;
	private String eoRepartAdresse = null;
	private String adresseAValider = null;
	private String adresseValdidee = null;
	private Boolean verifee = false;
	private Date dateMaj = null;
	private String codeReference = null;
	private String codeAVerifier = null;
	private Boolean codeStatus = null;
	
	public Adresse(String typeAdresse, String eoRepartAdresse, String adresseAValider, String adresseValdidee, Boolean verifee, NSTimestamp dateMaj, String codeReference) {
		this.typeAdresse = typeAdresse;
		this.eoRepartAdresse = eoRepartAdresse;
		this.adresseAValider = adresseAValider;
		this.adresseValdidee = adresseValdidee;
		this.verifee = verifee;
		this.dateMaj = dateMaj;
		this.codeReference = codeReference;
		this.codeAVerifier = "";
	}

	public String getTypeAdresse() {
		return typeAdresse;
	}

	public void setTypeAdresse(String typeAdresse) {
		this.typeAdresse = typeAdresse;
	}

	public String getEoRepartAdresse() {
		return eoRepartAdresse;
	}

	public void setEoRepartAdresse(String eoRepartAdresse) {
		this.eoRepartAdresse = eoRepartAdresse;
	}

	public String getAdresseAValider() {
		return adresseAValider;
	}

	public void setAdresseAValider(String adresseAValider) {
		this.adresseAValider = adresseAValider;
	}

	public String getAdresseValdidee() {
		return adresseValdidee;
	}

	public void setAdresseValdidee(String adresseValdidee) {
		this.adresseValdidee = adresseValdidee;
	}

	public Boolean getVerifee() {
		return verifee;
	}

	public void setVerifee(Boolean verifee) {
		this.verifee = verifee;
	}

	public Date getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(Date dateMaj) {
		this.dateMaj = dateMaj;
	}

	public String getCodeReference() {
		return codeReference;
	}

	public void setCodeReference(String codeReference) {
		this.codeReference = codeReference;
	}

	
	public String getCodeAVerifier() {
		return codeAVerifier;
	}

	public void setCodeAVerifier(String codeAVerifier) {
		this.codeAVerifier = codeAVerifier;
		System.out.println("this.codeAVerifier : " + this.codeAVerifier);
		
		if(this.codeAVerifier != null){
			try {
				verifierCode();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Boolean getCodeStatus() {
		return codeStatus;
	}

	public void setCodeStatus(Boolean codeStatus) {
		this.codeStatus = codeStatus;
	}

	public WOActionResults verifierCode(){
		codeStatus = false;
		System.out.println("Je suis dans verifierCode()");
		try {
			if(codeReference != null && codeAVerifier != null ){
				if(this.codeReference.equalsIgnoreCase(codage(codeAVerifier))){
					codeStatus = true;
					System.out.println("C'est le bon code");
				} else {
					codeStatus = false;
					System.out.println("Ce n'est pas le bon code");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String codage(String code) {
		byte[] encodedBytes = Base64.encodeBase64(code.getBytes());

		return new String(encodedBytes);
	}

}
