package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOContext;

import fr.univlr.dsi.fwknotifgui.ctrl.MailValidationPageCtrl;

public class MailValidationPage extends ANotifComponent {
	
	public MailValidationPage(WOContext context) {
		super(context);
	}

	public Class<?> getClassCtrl() {
		return MailValidationPageCtrl.class;
	}

	public MailValidationPageCtrl getCtrl() {
		return (MailValidationPageCtrl) super.getCtrl();
	}
}