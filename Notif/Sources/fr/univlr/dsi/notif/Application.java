package fr.univlr.dsi.notif;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;

import com.webobjects.appserver.WOMessage;
import com.webobjects.foundation.NSTimeZone;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXMessageEncoding;
import er.extensions.localization.ERXLocalizer;
import fr.univlr.dsi.fwknotif.INotifApplication;
//import fr.univlr.dsi.fwknotifgui.enhanced.NotificationsStore;
import fr.univlr.dsi.fwknotif.NotificationsStore;

public class Application extends CocktailAjaxApplication implements INotifApplication {

	public static void main(String[] argv) {
		ERXApplication.main(argv, Application.class);
	}
	
	
	private NotificationsStore store = new NotificationsStore();

	public Application() {

		// super();

		ERXApplication.log.info("Welcome to " + name() + " !");
		/* ** put your initialization code in here ** */
		// setAllowsConcurrentRequestHandling(true);
		setAllowsConcurrentRequestHandling(false);
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		setPageRefreshOnBacktrackEnabled(true);
		WOMessage.setDefaultEncoding("UTF-8");
		WOMessage.setDefaultURLEncoding("UTF-8");
		ERXMessageEncoding.setDefaultEncoding("UTF8");
		ERXMessageEncoding.setDefaultEncodingForAllLanguages("UTF8");

		// settage du timezone
		String tzs = config().stringForKey("DEFAULT_TIME_ZONE");
		if (tzs == null) {
			tzs = "CEST";
		}
		java.util.TimeZone tz = java.util.TimeZone.getTimeZone(tzs);
		java.util.TimeZone.setDefault(tz);
		NSTimeZone.setDefault(tz);

		ERXLocalizer.setDefaultLanguage("French");

	}

	@Override
	public String mainModelName() {
		// TODO Auto-generated method stub
		// nom du fichier Eco EOModel qu'il y a dans ressources
		return "Notif";
	}

	public String getApplicationURL() {
		String url = "";
		try {
			url = application().directConnectURL();
		} catch (Exception e) {
			System.out.println("Impossible de récupérer l'url de l'application");
		}
		return url;
	}

	@Override
	public NotificationsStore getNotificationStore() {
		
		return this.store;
	}
	
	
	
	
}
