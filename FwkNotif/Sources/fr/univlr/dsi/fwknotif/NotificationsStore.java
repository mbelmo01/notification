package fr.univlr.dsi.fwknotif;

import com.webobjects.foundation.NSMutableArray;

public class NotificationsStore {
public NSMutableArray<NotificationEnhanced> notificationsListe = new NSMutableArray<NotificationEnhanced>();

	

	public NotificationsStore() {
		super();
	}
	
	public NSMutableArray<NotificationEnhanced> getNotificationsListe() {
		return notificationsListe;
	}

	public void setNotificationsListe(NSMutableArray<NotificationEnhanced> notificationsListe) {
		this.notificationsListe = notificationsListe;
	}
	
	public void addNotification(NotificationEnhanced notification){
		this.notificationsListe.add(notification);
	}
	
	public void removeNotification(NotificationEnhanced notification){
		this.notificationsListe.remove(notification);
	}
}
