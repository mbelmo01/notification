package fr.univlr.dsi.notif.components;

import com.webobjects.appserver.WOContext;

import er.extensions.components.ERXComponent;
import fr.univlr.dsi.notif.Application;
import fr.univlr.dsi.notif.Session;

public class BaseComponent extends ERXComponent {
	public BaseComponent(WOContext context) {
		super(context);
	}
	
	@Override
	public Application application() {
		return (Application)super.application();
	}
	
	@Override
	public Session session() {
		return (Session)super.session();
	}
}
