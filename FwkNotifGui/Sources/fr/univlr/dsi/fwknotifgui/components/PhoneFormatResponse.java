package fr.univlr.dsi.fwknotifgui.components;

public class PhoneFormatResponse {
	private Boolean frenchCodeCountry = false;
	private Boolean frenchPhone = false;
	private Boolean frenchPhoneMobile = false;
	private Boolean validSize = false;
	private Boolean rep = false;

	public PhoneFormatResponse() {
		super();
	}

	public Boolean getFrenchCodeCountry() {
		return frenchCodeCountry;
	}

	public void setFrenchCodeCountry(Boolean frenchCodeCountry) {
		this.frenchCodeCountry = frenchCodeCountry;
	}

	public Boolean getFrenchPhone() {
		return frenchPhone;
	}

	public void setFrenchPhone(Boolean frenchPhone) {
		this.frenchPhone = frenchPhone;
	}

	public Boolean getFrenchPhoneMobile() {
		return frenchPhoneMobile;
	}

	public void setFrenchPhoneMobile(Boolean frenchPhoneMobile) {
		this.frenchPhoneMobile = frenchPhoneMobile;
	}

	public Boolean getValidSize() {
		return validSize;
	}

	public void setValidSize(Boolean validSize) {
		this.validSize = validSize;
	}

	public Boolean getRep() {
		return rep;
	}

	public void setRep(Boolean rep) {
		this.rep = rep;
	}

}
