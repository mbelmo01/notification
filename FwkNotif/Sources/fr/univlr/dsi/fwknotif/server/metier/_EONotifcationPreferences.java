// DO NOT EDIT.  Make changes to EONotifcationPreferences.java instead.
package fr.univlr.dsi.fwknotif.server.metier;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;

import java.math.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.cocktail.dt.server.metier.A_FwkDTRecord;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EONotifcationPreferences extends  A_FwkDTRecord {
  public static final String ENTITY_NAME = "NotifcationPreferences";
	public static final String ENTITY_TABLE_NAME = "HISTOTOX.NOTIFICATION_PREFERENCES";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> STRING_CONTENT = new ERXKey<String>("stringContent");
  public static final ERXKey<String> STRING_TYPE = new ERXKey<String>("stringType");
  // Relationship Keys

  // Attributes
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String STRING_CONTENT_KEY = STRING_CONTENT.key();
  public static final String STRING_TYPE_KEY = STRING_TYPE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EONotifcationPreferences.class);

  public EONotifcationPreferences localInstanceIn(EOEditingContext editingContext) {
    EONotifcationPreferences localInstance = (EONotifcationPreferences)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(_EONotifcationPreferences.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (_EONotifcationPreferences.LOG.isDebugEnabled()) {
    	_EONotifcationPreferences.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotifcationPreferences.D_MODIFICATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(_EONotifcationPreferences.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (_EONotifcationPreferences.LOG.isDebugEnabled()) {
    	_EONotifcationPreferences.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotifcationPreferences.PERS_ID_MODIFICATION_KEY);
  }

  public String stringContent() {
    return (String) storedValueForKey(_EONotifcationPreferences.STRING_CONTENT_KEY);
  }

  public void setStringContent(String value) {
    if (_EONotifcationPreferences.LOG.isDebugEnabled()) {
    	_EONotifcationPreferences.LOG.debug( "updating stringContent from " + stringContent() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotifcationPreferences.STRING_CONTENT_KEY);
  }

  public String stringType() {
    return (String) storedValueForKey(_EONotifcationPreferences.STRING_TYPE_KEY);
  }

  public void setStringType(String value) {
    if (_EONotifcationPreferences.LOG.isDebugEnabled()) {
    	_EONotifcationPreferences.LOG.debug( "updating stringType from " + stringType() + " to " + value);
    }
    takeStoredValueForKey(value, _EONotifcationPreferences.STRING_TYPE_KEY);
  }


  public static EONotifcationPreferences createNotifcationPreferences(EOEditingContext editingContext, NSTimestamp dModification
, Integer persIdModification
, String stringType
) {
    EONotifcationPreferences eo = (EONotifcationPreferences) EOUtilities.createAndInsertInstance(editingContext, _EONotifcationPreferences.ENTITY_NAME);    
		eo.setDModification(dModification);
		eo.setPersIdModification(persIdModification);
		eo.setStringType(stringType);
    return eo;
  }

  public static ERXFetchSpecification<EONotifcationPreferences> fetchSpec() {
    return new ERXFetchSpecification<EONotifcationPreferences>(_EONotifcationPreferences.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EONotifcationPreferences> fetchAllNotifcationPreferenceses(EOEditingContext editingContext) {
    return _EONotifcationPreferences.fetchAllNotifcationPreferenceses(editingContext, null);
  }

  public static NSArray<EONotifcationPreferences> fetchAllNotifcationPreferenceses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EONotifcationPreferences.fetchNotifcationPreferenceses(editingContext, null, sortOrderings);
  }

  public static NSArray<EONotifcationPreferences> fetchNotifcationPreferenceses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EONotifcationPreferences> fetchSpec = new ERXFetchSpecification<EONotifcationPreferences>(_EONotifcationPreferences.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EONotifcationPreferences> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EONotifcationPreferences fetchNotifcationPreferences(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotifcationPreferences.fetchNotifcationPreferences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotifcationPreferences fetchNotifcationPreferences(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EONotifcationPreferences> eoObjects = _EONotifcationPreferences.fetchNotifcationPreferenceses(editingContext, qualifier, null);
    EONotifcationPreferences eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one NotifcationPreferences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotifcationPreferences fetchRequiredNotifcationPreferences(EOEditingContext editingContext, String keyName, Object value) {
    return _EONotifcationPreferences.fetchRequiredNotifcationPreferences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EONotifcationPreferences fetchRequiredNotifcationPreferences(EOEditingContext editingContext, EOQualifier qualifier) {
    EONotifcationPreferences eoObject = _EONotifcationPreferences.fetchNotifcationPreferences(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no NotifcationPreferences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EONotifcationPreferences localInstanceIn(EOEditingContext editingContext, EONotifcationPreferences eo) {
    EONotifcationPreferences localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }


//Attributs non visibles
public static final String STRING_ORDRE_KEY = "stringOrdre";
public static final ERXKey<Integer> STRING_ORDRE = new ERXKey<Integer>("stringOrdre");

/**
 * Créer une instance de EONotifcationPreferences avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
//  public static  EONotifcationPreferences createEONotifcationPreferences(EOEditingContext editingContext//////////, NSTimestamp dModification
////////////////, Integer persIdModification
////////////////////, String stringType
//////////			) {
//    EONotifcationPreferences eo = (EONotifcationPreferences) createAndInsertInstance(editingContext, _EONotifcationPreferences.ENTITY_NAME);//    
////////////		eo.setDModification(dModification);
////////////////		eo.setPersIdModification(persIdModification);
////////////////////		eo.setStringType(stringType);
//////////    return eo;
//  }


}
