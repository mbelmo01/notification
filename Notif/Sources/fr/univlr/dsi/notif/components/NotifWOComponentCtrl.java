package fr.univlr.dsi.notif.components;

import com.webobjects.appserver.WOComponent;

import er.extensions.appserver.ERXRedirect;

public class NotifWOComponentCtrl {
	/**
	 * Supprimer l'URL spécifiant les paramètres CAS de l'URL. Pour éviter les
	 * problèmes de tickets non reconnus lors de la première connexion (appui
	 * sur F5, fleche précédente ...)
	 * 
	 * @param nextPage
	 * @return
	 */
	public final static WOComponent redirectWithoutCasCallbackUrl(WOComponent nextPage) {
		ERXRedirect redirect = (ERXRedirect) nextPage.pageWithName(ERXRedirect.class.getName());
		redirect.setComponent(nextPage);
		return redirect;
	}

}
