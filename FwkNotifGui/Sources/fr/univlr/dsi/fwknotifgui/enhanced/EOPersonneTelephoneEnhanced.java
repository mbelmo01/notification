package fr.univlr.dsi.fwknotifgui.enhanced;

import java.util.Comparator;
import java.util.List;

import javax.management.Notification;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.NotificationEnhanced;
import fr.univlr.dsi.fwknotif.NotificationsStore;
import fr.univlr.dsi.fwknotif.server.metier.NEOPersonneTelephone;
import fr.univlr.dsi.fwknotifgui.FwkNotifGui;
import fr.univlr.dsi.fwknotifgui.components.NotificationException;
import fr.univlr.dsi.fwknotifgui.components.NotificationUtils;
import fr.univlr.dsi.fwknotifgui.utils.TelephoneUtils;

public class EOPersonneTelephoneEnhanced {

	private static EOEditingContext ec = null;
	private static EOIndividu eoIndividu = null;
	private NEOPersonneTelephone eoPersonneTelephoneItem = null;

	private NSArray<EOPaysIndicatif> eoPaysIndicatifListe = new NSMutableArray<EOPaysIndicatif>();
	private EOPaysIndicatif eoPaysIndicatifItem = null;
	private EOPaysIndicatif eoPaysIndicatifSelection = null;

	private NSArray<EOTypeTel> eoTypeTelListe = new NSMutableArray<EOTypeTel>();
	private EOTypeTel eoTypeTelItem = null;
	private EOTypeTel eoTypeTelSelection = null;

	private String newNoTelephone = null;
	private Boolean isTelPrincipal = null;
	private Boolean telPrincipalCheckbox = null;
	
	
	private static INotifApplication application = null;
	
	public EOPersonneTelephoneEnhanced(EOEditingContext edc, EOIndividu individu, NSMutableArray<NEOPersonneTelephone> personneTelephoneListe, NEOPersonneTelephone personneTelephoneItem, INotifApplication app) {
		super();
		ec = edc;
		eoIndividu = individu;
		application = app;
		eoPaysIndicatifListe = new NSMutableArray<EOPaysIndicatif>(EOPaysIndicatif.fetchAll(ec));
		eoPersonneTelephoneItem = personneTelephoneItem;
		eoTypeTelItem = getTypeTel(personneTelephoneItem.typeTel());
		eoPaysIndicatifItem = getPaysIndicatifByIndicatif(eoPersonneTelephoneItem.indicatif());
		isTelPrincipal = false;
		
		if(eoPersonneTelephoneItem.telPrincipal().equalsIgnoreCase("O")){
			telPrincipalCheckbox = true;
		} else {
			telPrincipalCheckbox = false;
		}
		
		newNoTelephone = eoPersonneTelephoneItem.noTelephone();

		initEOPaysList();
		initEOTypeTelList();
	}
	
	public static EOIndividu getEoIndividu() {
		return eoIndividu;
	}

	public static void setEoIndividu(EOIndividu eoIndividu) {
		EOPersonneTelephoneEnhanced.eoIndividu = eoIndividu;
	}


	public NEOPersonneTelephone getEoPersonneTelephoneItem() {
		return eoPersonneTelephoneItem;
	}

	public void setEoPersonneTelephoneItem(NEOPersonneTelephone eoPersonneTelephoneItem) {
		this.eoPersonneTelephoneItem = eoPersonneTelephoneItem;
	}

	public NSArray<EOPaysIndicatif> getEoPaysIndicatifListe() {
		return eoPaysIndicatifListe;
	}

	public void setEoPaysIndicatifListe(NSArray<EOPaysIndicatif> eoPaysIndicatifListe) {
		this.eoPaysIndicatifListe = eoPaysIndicatifListe;
	}

	public EOPaysIndicatif getEoPaysIndicatifItem() {
		return eoPaysIndicatifItem;
	}

	public void setEoPaysIndicatifItem(EOPaysIndicatif eoPaysIndicatifItem) {
		this.eoPaysIndicatifItem = eoPaysIndicatifItem;
	}

	public EOPaysIndicatif getEoPaysIndicatifSelection() {
		return eoPaysIndicatifSelection;
	}

	public void setEoPaysIndicatifSelection(EOPaysIndicatif eoPaysIndicatifSelection) {
		this.eoPaysIndicatifSelection = eoPaysIndicatifSelection;
	}

	public NSArray<EOTypeTel> getEoTypeTelListe() {
		return eoTypeTelListe;
	}

	public void setEoTypeTelListe(NSArray<EOTypeTel> eoTypeTelListe) {
		this.eoTypeTelListe = eoTypeTelListe;
	}

	public EOTypeTel getEoTypeTelItem() {
		return eoTypeTelItem;
	}

	public void setEoTypeTelItem(EOTypeTel eoTypeTelItem) {
		this.eoTypeTelItem = eoTypeTelItem;
	}

	public EOTypeTel getEoTypeTelSelection() {
		return eoTypeTelSelection;
	}

	public void setEoTypeTelSelection(EOTypeTel eoTypeTelSelection) {
		this.eoTypeTelSelection = eoTypeTelSelection;
	}

	public String getNewNoTelephone() {
		return newNoTelephone;
	}

	public void setNewNoTelephone(String newNoTelephone) {
		this.newNoTelephone = newNoTelephone;
	}

//	public Boolean getIsTelPrincipal() {
//		System.out.println("isTelPrincipal : " + isTelPrincipal);
//		return isTelPrincipal;
//	}
//
//	public void setIsTelPrincipal(Boolean isTelPrincipal) {
//		System.out.println("isTelPrincipal : " + isTelPrincipal);
//		
//		if(isTelPrincipal == false){
//			if(canBeTelPrincipalUnchecked(eoPersonneTelephoneItem)){
//				this.isTelPrincipal = isTelPrincipal;
//			}
//		} else {
//			this.isTelPrincipal = isTelPrincipal;
//		}
//	}
	
	public Boolean getTelPrincipalCheckbox() {
		System.out.println(eoPersonneTelephoneItem.noTelephone() + " --> "+ telPrincipalCheckbox);
		return telPrincipalCheckbox;
	}

	public void setTelPrincipalCheckbox(Boolean telPrincipalCheckbox) {
		this.telPrincipalCheckbox = telPrincipalCheckbox;
		
		setTelPrincipal(eoPersonneTelephoneItem);
		
	}

	public void initEOPaysList() {

		// choix du pays indicatif par defaut
		eoPaysIndicatifListe = new NSArray<EOPaysIndicatif>();
		eoPaysIndicatifItem = null;
		eoPaysIndicatifSelection = null;
		try {
			eoPaysIndicatifListe = EOPaysIndicatif.fetchAll(ec);
			eoPaysIndicatifListe.sort(Comparator.comparing(EOPaysIndicatif::indicatif));
			eoPaysIndicatifSelection = getPaysIndicatifByIndicatif(eoPersonneTelephoneItem.indicatif());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void initEOTypeTelList() {
		// choix du pays indicatif par defaut
		eoTypeTelListe = new NSMutableArray<EOTypeTel>();
		eoTypeTelItem = null;
		eoTypeTelSelection = null;
		
		EOQualifier qual = null;
		try {
			
			if(eoIndividu.isEtudiant() && eoIndividu.isPersonnel()){
				qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("PRV", "PRF", "ETUD"));
			} else {
				if(eoIndividu.isEtudiant()){
					qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("ETUD"));
				}
				
				if(eoIndividu.isPersonnel()){
					qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("PRV", "PRF"));
				}
			}
			eoTypeTelListe.addAll(EOTypeTel.fetchAll(ec, qual, null));
			eoTypeTelListe.sort(Comparator.comparing(EOTypeTel::cTypeTel));
			eoTypeTelSelection = getTypeTel(eoPersonneTelephoneItem.typeTel());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EOPaysIndicatif getPaysIndicatifByIndicatif(Integer indicatif) {
		EOPaysIndicatif pays = null;

		try {
			EOQualifier qual = EOPaysIndicatif.INDICATIF.eq(indicatif);
			pays = EOPaysIndicatif.fetchFirstByQualifier(ec, qual);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pays;

	}

	public EOTypeTel getTypeTel(String cTypeTel) {
		EOTypeTel typeTel = null;
		
		try {
			EOQualifier qual = EOTypeTel.C_TYPE_TEL.eq(cTypeTel);
			typeTel = EOTypeTel.fetchFirstByQualifier(ec, qual);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return typeTel;
	}
	
	public void delete(){
		
		ec.deleteObject(eoPersonneTelephoneItem);
		try {
			NotificationUtils.save(ec);
		} catch (NotificationException e) {
			e.printStackTrace();
		}
	}

	public void update(){
		
//		Boolean rep = false;
		newNoTelephone = TelephoneUtils.cleanStringNumber(newNoTelephone);
			
		try {
			if(hasChanged()){
				if(canBeUpdated(newNoTelephone, eoPaysIndicatifSelection.indicatif())){
					eoPersonneTelephoneItem.setNoTelephone(newNoTelephone);
					eoPersonneTelephoneItem.setTypeTel(eoTypeTelSelection.cTypeTel());
					eoPersonneTelephoneItem.setIndicatif(eoPaysIndicatifSelection.indicatif());
					eoPersonneTelephoneItem.setDModification(new NSTimestamp());
					setTelPrincipal(eoPersonneTelephoneItem);
					
					NotificationUtils.save(ec);
					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.UpdatePhoneSuccess);
					application.getNotificationStore().addNotification(notificationEnhanced);
//					rep = true;
				} else{
					newNoTelephone = eoPersonneTelephoneItem.noTelephone();
				}
			}
			
		} catch (NotificationException e) {
			e.printStackTrace();
		}
		
//		return rep;
	}
	
	public Boolean canBeUpdated(String phone, Integer indicatif) {
		Boolean rep = false;
		Boolean doublons = false;
		Boolean longueur = false;
		Boolean prefixe = false;
		
//		doublons = verifierDoublon(eoTypeTelSelection.cTypeTel(), newNoTelephone);
		longueur = verifierLongueur(eoPaysIndicatifSelection.indicatif(), newNoTelephone);
		prefixe = verifierPrefixe(eoPaysIndicatifSelection.indicatif(), newNoTelephone);
		
		if((!doublons) && longueur && prefixe){
			rep = true;
		}
		
		return rep;
	}
	
	/*
	 * verifie si un numero de telephone est doublon. Pour etre un doublon, il faut présenter la même combinaison type + numéro. (de toute facon y a une contrainte en BDD qui emepche cette insertion).
	 */
	public Boolean verifierDoublon(String typeTel, String numero) {
		Boolean rep = false;

//		NSMutableArray<EOPersonneTelephoneEnhanced> liste = TelephoneUtils.fillTelephones(ec, eoIndividu, application);
//		for (EOPersonneTelephoneEnhanced eoPersonneTelephone : liste) {
//			if (numero.equalsIgnoreCase(eoPersonneTelephone.getEoPersonneTelephoneItem().noTelephone())) {
//				if (typeTel.equalsIgnoreCase(eoPersonneTelephone.getEoPersonneTelephoneItem().typeTel())) {
//					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.DuplicateTypeAndPhone);
//					application.getNotificationStore().addNotification(notificationEnhanced);
//					rep = true;
//				}
//			}
//		}
		
		List<ITelephone> liste = eoIndividu.toTelephones();
		
		for(ITelephone l : liste){
			if((numero.equalsIgnoreCase(l.noTelephone()))){
				if (typeTel.equalsIgnoreCase(l.toTypeTels().cTypeTel())) {
					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.DuplicateTypeAndPhone);
					application.getNotificationStore().addNotification(notificationEnhanced);
					rep = true;
				}
			}
		}

		return rep;
	}
	
	/*
	 * verifie que la longueur d'un numéro est correcte. Elle est entre 9 et 13 pour un numéro francais et 3 et 20 pour un numéro étranger. 
	 */
	public Boolean verifierLongueur(Integer indicatif, String numero) {
		Boolean rep = false;

		if (indicatif == 33) {
			if (numero.length() >= 9 && numero.length() <= 13) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.FrenchPhoneLenght);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		} else {
			if (numero.length() >= 3 && numero.length() <= 20) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.PhoneLenght);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		}
		return rep;
	}
		
	/*
	 * verifie si un numero commence bien par le bon prefixe : notamment en france, 06 ou 07 pour un mobile.
	 */
	public Boolean verifierPrefixe(Integer indicatif, String numero) {
		Boolean rep = false;

		String prefixe = numero.substring(0, 2);

		if (indicatif == 33) {
			if (prefixe.equalsIgnoreCase("06") || prefixe.equalsIgnoreCase("07")) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.FrenchPhonePrefixe);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		} else {
			rep = true;
		}

		return rep;
	}
	
	/*
	 * verifie si un numéro a changé. Verifie le type, l'indicatif et le numéro
	 */
	public Boolean hasChanged(){
		Boolean rep = false;
		
		if(!eoPersonneTelephoneItem.noTelephone().equalsIgnoreCase(newNoTelephone)){
			rep = true;
		}
		
		if(!eoPersonneTelephoneItem.indicatif().equals(eoPaysIndicatifSelection.indicatif())){
			rep = true;
		}
		
		if(!eoPersonneTelephoneItem.typeTel().equalsIgnoreCase(eoTypeTelSelection.cTypeTel())){
			rep = true;
		}
		
		String principal = "N";
		if(telPrincipalCheckbox == true){
			principal = "O";
		}
		if(!eoPersonneTelephoneItem.telPrincipal().equalsIgnoreCase(principal)){
			rep = true;
		}
		
		return rep;
	}
	
	
	public void setTelPrincipal(NEOPersonneTelephone eoPersonneTelephoneItem){
		
		NSMutableArray<NEOPersonneTelephone> tels = TelephoneUtils.fetchEOPersonneTelephones(ec, eoIndividu);
		if(telPrincipalCheckbox){
			for(NEOPersonneTelephone t : tels){
				t.setTelPrincipal("N");
				try {
					NotificationUtils.save(ec);
//					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.UpdateNumeroPrincipalSuccess);
//					application.getNotificationStore().addNotification(notificationEnhanced);
				} catch (NotificationException e) {
					e.printStackTrace();
				}
			}
			eoPersonneTelephoneItem.setTelPrincipal("O");
		} else {
			eoPersonneTelephoneItem.setTelPrincipal("N");
		}
		try {
			NotificationUtils.save(ec);
		} catch (NotificationException e) {
			e.printStackTrace();
		}
	}
	
	public static Boolean canBeTelPrincipalUnchecked(NEOPersonneTelephone eoPersonneTelephoneItem) {
		Boolean rep = false;
		NSMutableArray<NEOPersonneTelephone> tels = TelephoneUtils.fetchEOPersonneTelephones(ec, eoIndividu);
		
		
		tels.remove(eoPersonneTelephoneItem);
		for (NEOPersonneTelephone t : tels) {
			if (t.telPrincipal().equalsIgnoreCase("O")) {
				rep = true;
			}
		}
		
		if(rep == false){
			NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.NeedOnePrincpal);
			application.getNotificationStore().addNotification(notificationEnhanced);
		}
		return rep;
	}
	
	public String getCheckboxId(){
		String id = this.eoPersonneTelephoneItem.hashCode() + "";
		return id;
	}
	
	public String getObserverId(){
		String id = this.getCheckboxId()+"O";
		return id;
	}

	
}
