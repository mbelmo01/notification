package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.PreferencesCtrl;

public class Preferences extends ANotifComponent {
	
	private final static String BND_ctrl = "ctrl";
	
	public Preferences(WOContext context) {
		super(context);
	}

    public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	public Class<?> getClassCtrl() {
		return PreferencesCtrl.class;
	}

	public PreferencesCtrl getCtrl() {
		return (PreferencesCtrl) super.getCtrl();
	}

}