package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.WelcomePageCtrl;

public class WelcomePage extends ANotifComponent {

	private final static String BND_ctrl = "ctrl";

	public WelcomePage(WOContext context) {
		super(context);
	}

	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	public Class<?> getClassCtrl() {
		return WelcomePageCtrl.class;
	}

	public WelcomePageCtrl getCtrl() {
		return (WelcomePageCtrl) super.getCtrl();
	}

	public WOActionResults reload() {
		return this;
	}

	public WOActionResults goToForm() {

		return pageWithName(FormPage.class.getName());
	}

	public WOActionResults goToMailValidationPage() {

		return pageWithName(MailValidationPage.class.getName());
	}

	public WOActionResults goToAddressBook() {
		return pageWithName(AddressBook.class.getName());
	}
	
	public WOActionResults goToPreferences() {

		return pageWithName(Preferences.class.getName());
	}
}