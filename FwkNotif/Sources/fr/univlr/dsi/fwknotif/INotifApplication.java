package fr.univlr.dsi.fwknotif;

import com.webobjects.appserver.WOContext;


public interface INotifApplication {
	
	String getApplicationURL(WOContext context);
	
	NotificationsStore getNotificationStore();
}
