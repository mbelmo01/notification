package fr.univlr.dsi.fwknotifgui.components;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import er.extensions.foundation.ERXProperties;
import fr.univlr.dsi.fwknotifgui.utils.HtmlEmailSender;

public class NotificationException extends Exception {

	
	public NotificationException(EOIndividu individu, Exception e, String message) {
		try {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String errors = sw.toString();
			
			String from = ERXProperties.stringForKey("fr.univlr.dsi.notif.mail.exception.from");
			String to = ERXProperties.stringForKey("fr.univlr.dsi.notif.mail.exception.to");
			
			String body = "Message : " + message + "\n";
			body +=errors;
			
			
//			mailBus.sendMail(from, to, null, "Erreur Alerte-SMS : " + individu.getNomAndPrenom() + " ("+individu.noIndividuLogin()+")", body);
			e.printStackTrace();
			HtmlEmailSender.sendHtmlEmailWithDefaultParams(from, to, null, "Erreur Alerte-SMS : " + individu.getNomAndPrenom() + " ("+individu.noIndividuLogin()+")", body);
			
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
}
