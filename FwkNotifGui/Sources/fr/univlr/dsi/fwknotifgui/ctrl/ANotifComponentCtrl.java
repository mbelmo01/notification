package fr.univlr.dsi.fwknotifgui.ctrl;

import java.text.Normalizer;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.eocontrol.EOEditingContext;

import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.INotifSession;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;


public abstract class ANotifComponentCtrl {
	/*
	 * ------------------------------------------------------------------------
	 * ATTRIBUTES
	 * ------------------------------------------------------------------------
	 */
	private final ANotifComponent component;
	

	private final static Logger log = Logger.getLogger(ANotifComponentCtrl.class);

	public ANotifComponentCtrl(ANotifComponent component) {
		super();
		this.component = component;

	}

	public ANotifComponent getComponent() {
		return component;
	}

	public final INotifApplication application() {
		return getComponent().notifApplication();
	}

	public final INotifSession session() {
		return getComponent().notifSession();
	}

	public final EOEditingContext editingContext() {
		return session().defaultEditingContext();
	}

	public CktlMailBus mailBus() {
		return session().mailBus();
	}

	public Boolean getisAdmin() {
		return session().isAdmin();
	}

	public static String chaineSansAccents(String chaine) {
		return Normalizer.normalize(chaine, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
	}

	public String codage(String code) {
		byte[] encodedBytes = Base64.encodeBase64(code.getBytes());

		return new String(encodedBytes);
	}

}
