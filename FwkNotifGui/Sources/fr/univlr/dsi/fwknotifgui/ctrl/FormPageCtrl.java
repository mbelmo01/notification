package fr.univlr.dsi.fwknotifgui.ctrl;

import java.awt.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.dsi.fwknotif.NotificationEnhanced;
import fr.univlr.dsi.fwknotif.NotificationsStore;
import fr.univlr.dsi.fwknotif.server.metier.EONotificationUserPreferences;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;
import fr.univlr.dsi.fwknotifgui.components.EORepartPersonneAdresseEnhanced;
import fr.univlr.dsi.fwknotifgui.components.NotificationException;
import fr.univlr.dsi.fwknotifgui.components.NotificationUtils;
import fr.univlr.dsi.fwknotifgui.components.PreferencesUtils;
import fr.univlr.dsi.fwknotifgui.enhanced.EOPersonneTelephoneEnhanced;
import fr.univlr.dsi.fwknotifgui.enhanced.PersonneTelephoneFactory;
import fr.univlr.dsi.fwknotifgui.utils.TelephoneUtils;

public class FormPageCtrl extends ANotifComponentCtrl {

	public final static Logger log = Logger.getLogger(AddressBookCtrl.class);
	public EOEditingContext ec = null;
	public EOIndividu connectedIndividu = null;
	private NotificationEnhanced notificationEnhanced = null;

	public String explication = null;
	public String title = null;
	public String subtitle = null;
	public PreferencesUtils prefs = null;
	public PersonneTelephoneFactory personneTelephoneFactory = null;

	private NSMutableArray<EOPersonneTelephoneEnhanced> eoPersonneTelephoneListe = new NSMutableArray<EOPersonneTelephoneEnhanced>();
	private EOPersonneTelephoneEnhanced eoPersonneTelephoneItem = null;
	public NotificationsStore localStore = new NotificationsStore();

	
	
	public FormPageCtrl(ANotifComponent component) throws NotificationException {
		super(component);
		this.ec = session().defaultEditingContext();
		this.connectedIndividu = EOIndividu.individuWithPersId(ec, session().applicationUser().getPersId());
		this.prefs = new PreferencesUtils(ec);
		this.explication = prefs.getExplication(session().localizer().languageCode()).stringContent();
		this.title = prefs.getTitle(session().localizer().languageCode()).stringContent();
		this.subtitle = prefs.getSubTitle(session().localizer().languageCode()).stringContent();
		this.initPhoneForm();
		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());
		
	}

	public Boolean getNotification() {
		return EONotificationUserPreferences.getUserNotificationPreferenceValue(ec, connectedIndividu.persId());
	}

	public void setNotification(Boolean notification) {
		EONotificationUserPreferences.setUserNotificationPreference(ec, connectedIndividu.persId(), notification);
	}

	public EOPersonneTelephoneEnhanced getEoPersonneTelephoneItem() {
		return eoPersonneTelephoneItem;
	}

	public void setEoPersonneTelephoneItem(EOPersonneTelephoneEnhanced eoPersonneTelephoneItem) {
		this.eoPersonneTelephoneItem = eoPersonneTelephoneItem;
	}

	public NSMutableArray<EOPersonneTelephoneEnhanced> getEoPersonneTelephoneListe() {
		EOPersonneTelephone p = null;
		return eoPersonneTelephoneListe;
	}

	public WOActionResults initPhoneForm() {

		eoPersonneTelephoneListe = new NSMutableArray<EOPersonneTelephoneEnhanced>();
		eoPersonneTelephoneListe = TelephoneUtils.fillTelephones(ec, connectedIndividu, application());
		this.personneTelephoneFactory = new PersonneTelephoneFactory(ec, connectedIndividu, application());

		return null;
	}

	public WOActionResults updatePhoneItem() {

		eoPersonneTelephoneItem.update();
		initPhoneForm();
		return null;
	}

	public WOActionResults deletePhoneItem() {

		eoPersonneTelephoneItem.delete();
		initPhoneForm();
		return null;
	}

	public Boolean displayPersonneTelephoneFactory() {
		Boolean rep = false;

		if (eoPersonneTelephoneListe.count() < prefs.getMaxNumber(ec)) {
			rep = true;
		}

		return rep;
	}

	public String indicatifWithCountryName() {
		String str = eoPersonneTelephoneItem.getEoPaysIndicatifItem().indicatif().toString();

		if (eoPersonneTelephoneItem.getEoPaysIndicatifItem() != null) {
			try {
				str += " -- " + eoPersonneTelephoneItem.getEoPaysIndicatifItem().toPays().llPays();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return str;
	}

	public WOActionResults addPersonneTelephone() {
		personneTelephoneFactory.addPersonneTelephone();
		initPhoneForm();
		return null;
	}

	/**********************************
	 * FONCTIONS MAIL
	 **********************************/

	public NSMutableArray<EORepartPersonneAdresseEnhanced> adresses = null;
	public EORepartPersonneAdresseEnhanced adresse = null;
	public NSArray<String> domainesInterdits = new NSArray<String>();
	public Boolean isValidDomain = null;
	public Boolean displayWarningPopUp = null;

	public WOActionResults updateeMail() throws Exception {

		this.adresse.update();
		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());
		return null;
	}

	public WOActionResults validerEmail() {

		this.adresse.validerAdresse();
		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());

		return null;
	}

	/*
	 * permet de supprimer une adresse mail
	 */
	public WOActionResults deleteMail() {
		adresse.deleteEmail();
		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());
		return null;
	}

	/*
	 * permet de supprimer une adresse mail actuellement valide
	 */
	public WOActionResults deleteEmailEnCours() {
		adresse.deleteEmailEnCours();
		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());
		return null;
	}

	/*
	 * permet de rafraichir la liste des adresses mails.
	 */
	public WOActionResults refreshDataMail() {

		this.adresses = NotificationUtils.fillAdresses(ec, connectedIndividu, session(), application());
		return null;
	}

	public NotificationEnhanced getNotificationEnhanced() {
		if (!notificationEnhanced.getHasBeenRead()) {
			notificationEnhanced.setHasBeenRead(true);
			return notificationEnhanced;
		} else {
			return null;
		}

	}

	public void setNotificationEnhanced(NotificationEnhanced notificationEnhanced) {
		this.notificationEnhanced = notificationEnhanced;
	}
	
	public NSMutableArray<String> updateContainer1() {
//		return new NSMutableArray<String>(new String[] { "container1", "container21" });
		return new NSMutableArray<String>(new String[] { "container1"});
	}
	
	
	private int numRequests = 0;
	public WOActionResults longRunningAction() throws InterruptedException{
    	System.out.println("AjaxBusyIndicatorExample.longRunningAction()");
    	Thread.sleep(3000);
    	numRequests+=1;
    	return null;
    }

	public int getNumRequests() {
		return numRequests;
	}

	public void setNumRequests(int numRequests) {
		this.numRequests = numRequests;
	}
	
	public NSTimestamp now(){
		return new NSTimestamp();
	}
	
	

}
