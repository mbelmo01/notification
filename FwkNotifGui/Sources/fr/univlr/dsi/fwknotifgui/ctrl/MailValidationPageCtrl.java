package fr.univlr.dsi.fwknotifgui.ctrl;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import fr.univlr.dsi.fwknotif.server.metier.EONotificationMailConf;
import fr.univlr.dsi.fwknotifgui.components.ANotifComponent;
import fr.univlr.dsi.fwknotifgui.components.PreferencesUtils;

public class MailValidationPageCtrl extends ANotifComponentCtrl {

	EOEditingContext ec = session().defaultEditingContext();
	private EOIndividu individu = null;
	private String uuid = null;
	private String mail = null;
	private boolean displayInvalidLink = false;
//	private EmailInfo emailInfo = null;
	private PreferencesUtils pref = null;
	
	public MailValidationPageCtrl(ANotifComponent component) {
		super(component);
//		this.emailInfo = new EmailInfo(ec);
		this.pref = new PreferencesUtils(ec);
		try {
			individu = session().applicationUser().getEoIndividu();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getAppUrl() {
		String url = null;
		url = application().getApplicationURL(session().context());
		return url;
	}

	public EOEditingContext getEc() {
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
	}

	public EOIndividu getIndividu() {
		return individu;
	}

	public void setIndividu(EOIndividu individu) {
		this.individu = individu;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {

		System.out.println("J'ai bien recu l'uid dans le controller " + uuid);
		this.uuid = uuid;

		if (uuid != null) {
			validation(uuid);
		}
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isDisplayInvalidLink() {
		return displayInvalidLink;
	}

	public void setDisplayInvalidLink(boolean displayInvalidLink) {
		this.displayInvalidLink = displayInvalidLink;
	}

	public void validation(String uuid) {
		EOQualifier qual1 = EONotificationMailConf.UUID.eq(uuid);

		EONotificationMailConf mailConf = EONotificationMailConf.fetchNotificationMailConf(ec, qual1);

		if (mailConf != null) {
			individu = EOIndividu.individuWithPersId(ec, mailConf.mailConfPersId());
			EOQualifier qual2 = EORepartPersonneAdresse.ADR_ORDRE.eq(mailConf.mailConfAdrOrdre());
			EOQualifier qual3 = EORepartPersonneAdresse.TADR_CODE.eq(mailConf.mailConfTadrCode());

			EOQualifier qualifierFinale = ERXQ.and(qual2, qual3);

			EORepartPersonneAdresse add = EORepartPersonneAdresse.fetchByQualifier(ec, qualifierFinale);

			mail = mailConf.mailAValider();

			if (mailConf.uuidDFin().after(new NSTimestamp())) {
				
				// ce if sert à empecher que quelqu'un copie le lien et valide une adresse...
				if(individu.equals(EOIndividu.individuWithPersId(ec, mailConf.mailConfPersId()))){
					try {
						if (mailConf.mailAValider() != null) {
							add.setEMail(mailConf.mailAValider());
							mailConf.setMailValide(mailConf.mailAValider());
							mailConf.setMailAValider(null);
							mailConf.setMailConfValideStatus("O");
							mailConf.setDModification(new NSTimestamp());

						}

						save();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// l'invididu qui utilise le lien n'est pas la bonne personne connecté.
					displayInvalidLink = true;
				}
				
			} else {
				displayInvalidLink = true;
			}
		} else {
			displayInvalidLink = true;
		}
	}

	public void save() {
		ec.lock();

		try {
			ec.saveChanges();
		} catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}

		finally {
			ec.unlock();
		}
	}

}
