package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.ANotifComponentCtrl;

public class FormPageInformations extends ANotifComponent {
	public FormPageInformations(WOContext context) {
		super(context);
	}

	private final static String BND_explication = "explication";

	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
	}

	public final String getExplication() {
		return (String) valueForBinding(BND_explication);
	}

	public ANotifComponentCtrl getCtrl() {
		return (ANotifComponentCtrl) super.getCtrl();
	}

	@Override
	public Class<?> getClassCtrl() {
		return ANotifComponentCtrl.class;

	}
}