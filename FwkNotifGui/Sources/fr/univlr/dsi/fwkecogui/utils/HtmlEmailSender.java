package fr.univlr.dsi.fwkecogui.utils;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import er.extensions.foundation.ERXProperties;

public class HtmlEmailSender {

	private String host, port, userName, password, toAddress, subject, message;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static void sendHtmlEmail(String host, String port, final String userName, final String password,
			String toAddress,String ccAddress, String subject, String message) throws AddressException, MessagingException {

		/*
		 * sets SMTP server properties creates a new authenticator creates a new
		 * session with an authenticator creates a new e-mail message sends the
		 * e-mail
		 */
		Properties properties = setProperties(host, port);
		Authenticator auth = setAuthenticator(userName, password);
		Session session = Session.getInstance(properties, auth);
		Message msg = setMessageProperties(session, userName, toAddress, ccAddress, subject, message);
		Transport.send(msg);

	}

	public static void sendHtmlEmailWithDefaultParams(String from, String to, String cc, String subject, String message)
			throws AddressException, MessagingException {

		/*
		 * sets SMTP server properties creates a new authenticator creates a new
		 * session with an authenticator creates a new e-mail message sends the
		 * e-mail
		 */
		String host = ERXProperties.stringForKey("fr.univlr.dsi.eco.mail.host");
		String port = ERXProperties.stringForKey("fr.univlr.dsi.eco.mail.port");
//		String password = ERXProperties.stringForKey("fr.univlr.dsi.eco.mail.password");
		String password = "";
		System.out.println("host : " + host);
		System.out.println("port : " + port);
		System.out.println("from : " + from);
		System.out.println("password : " + password);
		System.out.println("to : " + to);
		System.out.println("cc : " + cc);

		Properties properties = setProperties(host, port);

		properties.put("mail.smtp.auth", "false");
		Authenticator auth = setAuthenticator(from, password);
		Session session = Session.getInstance(properties, auth);
		Message msg = setMessageProperties(session, from, to, cc, subject, message);;
		Transport.send(msg);

	}

	public static Properties setProperties(String host, String port) {

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "flase");
		properties.put("mail.smtp.starttls.enable", "true");

		return properties;
	}

	public static Authenticator setAuthenticator(final String userName, final String password) {

		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		};
		return auth;
	}

	public static Message setMessageProperties(Session session, String userName, String toAddress, String ccAddress, String subject,
			String message) throws AddressException, MessagingException {

		Message msg = new MimeMessage(session);

		msg.setFrom(new InternetAddress(userName));
		InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		
		
		if(ccAddress != null){
			InternetAddress[] ccAddresses = { new InternetAddress(ccAddress) };
			msg.addRecipients(Message.RecipientType.CC, ccAddresses);
		}
		
		
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		

		// set plain text message
		msg.setContent(message, "text/html; charset=utf-8");
		return msg;
	}
}
