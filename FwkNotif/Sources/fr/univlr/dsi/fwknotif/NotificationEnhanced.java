package fr.univlr.dsi.fwknotif;

public class NotificationEnhanced {
	private String type = null;
	private String title = null;
	private String text = null;
	private String style = null;
	private String stack = null;
	private String position = null;
	private Boolean hasBeenRead = null;

	public static final String FrenchPhoneLenght = "FRENCH_PHONE_LENGHT";
	public static final String PhoneLenght = "PHONE_LENGHT";
	public static final String FrenchPhonePrefixe = "PHONE_PREFIXE";
	public static final String PhoneDuplicate = "PHONE_DUPLICATE";
	public static final String DuplicateTypeAndPhone = "PHONE_DUPLICATE_TYPE_PHONE";
	public static final String UpdatePhoneSuccess = "UPDATE_PHONE_SUCCESS";
	public static final String AddPhoneSuccess = "ADD_PHONE_SUCCESS";
	public static final String UpdateMailSuccess = "UPDATE_MAIL_SUCCESS";
	public static final String MailCodeValidationError = "MAIL_CODE_VALIDATION_ERROR";
	public static final String NeedOnePrincpal = "PHONE_NEED_ONE_PRINCIPAL";
	public static final String UpdateNumeroPrincipalSuccess = "PHONE_PRINCIPAL_UPDATE_SUCCESS";
	


	public NotificationEnhanced(String type, String title, String text, String style, String stack, String position) {
		this.type = type;
		this.title = title;
		this.text = text;
		this.style = style;
		this.stack = stack;
		this.position = position;
		hasBeenRead = false;
	}
	
	public NotificationEnhanced(String type) {
		
		if(type.equalsIgnoreCase(FrenchPhoneLenght)){
			this.type = "error";
			this.title = "Numéro de téléphone incorrecte";
			this.text = "La longueur d'un numéro français doit être comprise entre 10 et 13 chiffres.";
			this.style = "error";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(PhoneLenght)){
			this.type = "error";
			this.title = "Numéro de téléphone incorrecte";
			this.text = "La longueur d'un numéro étranger doit être comprise entre 2 et 20.";
			this.style = "error";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(PhoneDuplicate)){
			this.type = "";
			this.title = "Numéro de téléphone connu";
			this.text = "Le numéro de téléphone saisi est déjà renseigné";
			this.style = "error";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(FrenchPhonePrefixe)){
			this.type = "error";
			this.title = "Numéro de téléphone incorrecte";
			this.text = " Pour un numéro français (indicatif +33), merci de renseigner un numéro qui commence par 06 ou 07.";
			this.style = "error";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(UpdatePhoneSuccess)){
			this.type = "success";
			this.title = "Mise à jour réussie";
			this.text = " Votre numéro de téléphone a bien été mis à jour.";
			this.style = "success";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(AddPhoneSuccess)){
			this.type = "success";
			this.title = "Sauvegarde réussie";
			this.text = " Votre numéro de téléphone a bien été ajouté.";
			this.style = "success";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		// il n'est pas possible d'avoir deux fois la meme combinaison "type + numéro"
		if(type.equalsIgnoreCase(DuplicateTypeAndPhone)){
			this.type = "";
			this.title = "Non autorisé";
			this.text = " Vous ne pouvez avoir deux fois la même combinaison catégorie / numéro";
			this.style = "success";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(UpdateMailSuccess)){
			this.type = "success";
			this.title = "Mise à jour réussie";
			this.text = " Votre adresse mail de contact a bien été mise à jour.";
			this.style = "success";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		if(type.equalsIgnoreCase(MailCodeValidationError)){
			this.type = "error";
			this.title = "Code incorrect";
			this.text = " Le code saisi n'est pas celui attentu.";
			this.style = "error";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		if(type.equalsIgnoreCase(NeedOnePrincpal)){
			this.type = "";
			this.title = "Non autorisé";
			this.text = " Au moins un numéro de téléphone doit être défini en tant que principal.";
			this.style = "";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
		
		if(type.equalsIgnoreCase(UpdateNumeroPrincipalSuccess)){
			this.type = "success";
			this.title = "Mise à jour réussie";
			this.text = " Numéro de téléphone princpal mis à jour.";
			this.style = "";
			this.stack = "myStack";
			this.position = position;
			hasBeenRead = false;
		}
	}

	/*
	 * 
	 * var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
	 * 	new PNotify({
	 * 		type: "error",
	 * 		title: "Erreur",
	 * 		text: "mon message",
	 * 		addclass: "error",
	 * 		stack: myStack
	 * 	})
	 */

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getStack() {
		return stack;
	}

	public void setStack(String stack) {
		this.stack = stack;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Boolean getHasBeenRead() {
		return hasBeenRead;
	}

	public void setHasBeenRead(Boolean hasBeenRead) {
		this.hasBeenRead = hasBeenRead;
	}

	public String toJavascript() {
		
		String javascript = "";
	
		javascript += "var myStack = {\"dir1\":\"down\", \"dir2\":\"right\", \"push\":\"top\"};";
		
		javascript += "new PNotify({";

		javascript += "type: \"" + this.type + "\",";
		
		javascript += "title: \"" + this.title + "\",";

		javascript += "text: \"" + this.text + "\",";

		javascript += "addclass: \"" + this.style + "\",";

		javascript += "stack: " + this.stack;
		
		javascript += "})";
		
		
		return javascript;
	}
	
	
	public String toSimpleJavascript() {
		
		String javascript = "";
	
//		javascript += "var myStack = {\"dir1\":\"down\", \"dir2\":\"right\", \"push\":\"top\"};";
		
		javascript += "new PNotify({";
		
		javascript += "title: \"" + this.title + "\",";

		javascript += "text: \"" + this.text + "\",";

		javascript += "type: \"" + this.type + "\",";
		
		javascript += "})";
		
		
		return javascript;
	}
}
