package fr.univlr.dsi.fwknotifgui.components;

public class PersonneTelephoneTmp {

	Integer persId = null;
	Integer indicatif = null;
	String numero = null;
	String typeTel = null;
	String typeNo = null;

	public PersonneTelephoneTmp(Integer persId, Integer indicatif, String numero, String typeTel, String typeNo) {
		super();
		this.persId = persId;
		this.indicatif = indicatif;
		this.numero = numero;
		this.typeTel = typeTel;
		this.typeNo = typeNo;
	}

	public Integer getPersId() {
		return persId;
	}

	public void setPersId(Integer persId) {
		this.persId = persId;
	}

	public Integer getIndicatif() {
		return indicatif;
	}

	public void setIndicatif(Integer indicatif) {
		this.indicatif = indicatif;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTypeTel() {
		return typeTel;
	}

	public void setTypeTel(String typeTel) {
		this.typeTel = typeTel;
	}

	public String getTypeNo() {
		return typeNo;
	}

	public void setTypeNo(String typeNo) {
		this.typeNo = typeNo;
	}

	public Boolean equals(PersonneTelephoneTmp tmp) {
		Boolean rep = false;

		if (
				(this.persId.equals(tmp.persId)) && (this.indicatif.equals((tmp.indicatif))
				&&
				(this.numero.equalsIgnoreCase(tmp.numero)) && (this.typeTel.equalsIgnoreCase(tmp.typeTel)))
//				&&
//				(this.typeNo.equalsIgnoreCase(tmp.typeNo))
				
				) {
			rep = true;
		}

		return rep;
	}
}
