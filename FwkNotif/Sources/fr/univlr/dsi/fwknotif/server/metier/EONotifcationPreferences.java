package fr.univlr.dsi.fwknotif.server.metier;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EONotifcationPreferences extends _EONotifcationPreferences {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EONotifcationPreferences.class);

	public EONotifcationPreferences() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        this.setDModification(new NSTimestamp());
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele a partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
