package fr.univlr.dsi.fwknotifgui.components;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOContext;

import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.INotifSession;
import fr.univlr.dsi.fwknotifgui.ctrl.ANotifComponentCtrl;

public abstract class ANotifComponent extends CktlWebComponent {

	// binding très courant
	private final static String BND_updateContainerID = "updateContainerID";

	private final ANotifComponentCtrl ctrl;

	public ANotifComponent(WOContext context) {
		super(context);
		ctrl = instancierCtrl();
	}

	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	public ANotifComponentCtrl getCtrl() {
		return ctrl;
	}

	public INotifApplication notifApplication() {
		return (INotifApplication) cktlApp();
	}

	public INotifSession notifSession() {
		return (INotifSession) cktlSession();
	}

	public final String getUpdateContainerID() {
		return (String) valueForBinding(BND_updateContainerID);
	}

	public abstract Class<?> getClassCtrl();

	private final ANotifComponentCtrl instancierCtrl() {
		ANotifComponentCtrl newCtrl = null;

		Constructor<ANotifComponentCtrl> constuctor = null;
		try {
			
			constuctor = (Constructor<ANotifComponentCtrl>) getClassCtrl().getConstructor(new Class[] { ANotifComponent.class });
			
			try {
				newCtrl = constuctor.newInstance(new Object[] { this });
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}

		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return newCtrl;
	}

}
