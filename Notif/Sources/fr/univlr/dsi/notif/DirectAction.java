package fr.univlr.dsi.notif;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlLogin;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WORequest;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEC;
import fr.univlr.dsi.fwknotif.NotifUserInfo;
import fr.univlr.dsi.fwknotifgui.components.FormPage;
import fr.univlr.dsi.fwknotifgui.components.MailValidationPage;
import fr.univlr.dsi.fwknotifgui.components.WelcomePage;

public class DirectAction extends CktlWebAction {

	private String loginComment;

	public DirectAction(WORequest request) {
		super(request);
	}

	@Override
	public Session session() {
		getSessionIDForRequest(context().request());
		context().request().cookies();

		Session notifSession = (Session) existingSession();
		if (notifSession == null) {
			notifSession = (Session) super.session();
		}
		return notifSession;
	}

	public Session notifSession() {

		getSessionIDForRequest(context().request());
		context().request().cookies();

		Session notifSession = (Session) existingSession();
		if (notifSession == null) {
			notifSession = (Session) session();
		}
		return notifSession;
	}

	public Application notifApplication() {
		return (Application) WOApplication.application();
	}

	@Override
	public WOActionResults defaultAction() {
		if (useCasService())
			return loginCASPage();
		else
			return loginNoCasPage(null);
	}

	/**
	 * CAS : traitement authentification OK
	 */
	public WOActionResults loginCasSuccessPage(String netid) {
		try {
			CktlLog.log("loginCasSuccessPage : " + netid);
			ERXEC edc = (ERXEC) session().defaultEditingContext();
			IPersonne pers = PersonneDelegate.fetchPersonneForLogin(edc, netid);
			if (pers == null) {
				throw new Exception("Impossible de recuperer un objet personne associé au login " + netid);
			}
			CktlLog.log("loginCasSuccessPage : " + pers.persLibelle() + " " + pers.persLc());
			NotifUserInfo appUser = new NotifUserInfo(edc, Integer.valueOf(pers.persId().intValue()));

			// exclusion de ceux non concernés
			if (appUser.isAccesAutorise() == false) {
				throw new Exception("Vous n'êtes pas autorisé(e) à utiliser cette application");
			}
			session().setApplicationUser(appUser);
			// si admin, alors accueil, sinon document unique
			WOComponent nextPage = null;
			nextPage = null;

			if (appUser.isAdmin(appUser.getPersId())) {
				nextPage = session().getSavedPageWithName(WelcomePage.class.getName());
			} else {
				nextPage = session().getSavedPageWithName(FormPage.class.getName());
			}
			return nextPage;

		} catch (Exception e) {
			e.printStackTrace();
			return getErrorPage(e.getMessage());
		}
	}

	@Override
	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		return loginCasSuccessPage(netid);
	}

	@Override
	public WOActionResults loginCasFailurePage(String errorMessage, String errorCode) {
		CktlLog.log("loginCasFailurePage : " + errorMessage + " (" + errorMessage + ")");

		StringBuffer msg = new StringBuffer();
		msg.append("Une erreur s'est produite lors de l'authentification de l'uilisateur&nbsp;:<br><br>");
		if (errorMessage != null)
			msg.append("&nbsp;:<br><br>").append(errorMessage);
		return getErrorPage(msg.toString());
	}

	@Override
	public WOActionResults loginNoCasPage(NSDictionary actionParams) {
		return getErrorPage("Authentification CAS obligatoire");
	}

	public WOComponent getErrorPage(String errorMessage) {
		CktlAlertPage page = (CktlAlertPage) cktlApp.pageWithName(CktlAlertPage.class.getName(), context());
		page.showMessage(null, cktlApp.name() + " : ERREUR", errorMessage, null, null, null, CktlAlertPage.ERROR, null);
		return page;
	}

	public WOActionResults loginCASPage() {
		String url = DirectAction.getLoginActionURL(this.context(), false, null, true, null);
		WORedirect page = (WORedirect) pageWithName(WORedirect.class.getName());
		page.setUrl(url);
		return page;
	}

	public void setLoginComment(String comment) {
		loginComment = comment;
	}

	public String casLoginLink() {
		return null;
	}

	public CktlLoginResponder getNewLoginResponder(NSDictionary actionParams) {
		return new DefaultLoginResponder(actionParams);
	}

	public class DefaultLoginResponder implements CktlLoginResponder {
		private NSDictionary actionParams;

		public DefaultLoginResponder(NSDictionary actionParams) {
			this.actionParams = actionParams;
		}

		public NSDictionary actionParams() {
			return actionParams;
		}

		public WOComponent loginAccepted(CktlLogin loginComponent) {
			WOComponent nextPage = null;

			Session session = (Session) loginComponent.session();
			CktlLog.trace("session ID : " + session.sessionID());
			CktlLog.trace("" + loginComponent.loggedUserInfo());

			try {
				// nextPage = (WOComponent) doConnect(session,
				// loginComponent.loggedUserInfo().login());
				nextPage = (WOComponent) pageWithName(MailValidationPage.class);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return nextPage;
		}

		public boolean acceptLoginName(String loginName) {
			return cktlApp.acceptLoginName(loginName);
		}

		public boolean acceptEmptyPassword() {
			return cktlApp.config().booleanForKey("ACCEPT_EMPTY_PASSWORD");
		}

		public String getRootPassword() {
			return cktlApp.getRootPassword();
		}
	}
}
