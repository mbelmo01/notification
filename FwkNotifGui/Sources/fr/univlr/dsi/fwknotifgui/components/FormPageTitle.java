package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.ANotifComponentCtrl;
import fr.univlr.dsi.fwknotifgui.ctrl.FormPageCtrl;

public class FormPageTitle extends ANotifComponent {

	private final static String BND_title = "title";
	private final static String BND_subTitle = "subTitle";

	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
	}

	public FormPageTitle(WOContext context) {
		super(context);
	}

	public final String getTitle() {
		return (String) valueForBinding(BND_title);
	}

	public final String getSubTitle() {
		return (String) valueForBinding(BND_subTitle);
	}

	public FormPageCtrl getCtrl() {
		return (FormPageCtrl) super.getCtrl();
	}

	@Override
	public Class<?> getClassCtrl() {
		return FormPageCtrl.class;

	}
}