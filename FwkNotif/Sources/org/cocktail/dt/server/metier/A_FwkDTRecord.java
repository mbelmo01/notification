package org.cocktail.dt.server.metier;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.common.database.CktlRecord;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe abstraite pour les entités métier
 * 
 * @author Xxxx YYYY <xxx.yyyy at cocktail.org>
 */

public abstract class A_FwkDTRecord extends CktlRecord {

	public static final String OUI = "O";
	public static final String NON = "N";

	private NSArray attributes = null;
	private Map attributesTaillesMax;
	private NSArray attributesObligatoires;

	/**
	 * @return La date actuelle.
	 */
	public NSTimestamp now() {
		return new NSTimestamp(new Date());
	}

	/**
	 * Verifie si la longueur d'une chaine est bien inferieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 *            -1 si illimite.
	 * @return true si l=-1 ou si le nombre de caracteres de s est inferieur ou
	 *         egal a l.
	 */
	protected boolean verifieLongueurMax(String s, int l) {
		return (l == -1 || s == null || s.trim().length() <= l);
	}

	/**
	 * Verifie si la longeur d'une chaine est bien superieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 * @return true si l=0 ou si le nombre de caracteres de s est superieur ou
	 *         egal a l.
	 */
	protected boolean verifieLongueurMin(String s, int l) {
		return (s == null || s.trim().length() >= l);
	}

	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim). A appeler
	 * en debut de ValidateObjectMetier.
	 */
	protected void trimAllString() {
		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String array_element = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(array_element) != null
					&& obj.valueForKey(array_element) instanceof String) {
				obj.takeValueForKey(((String) obj.valueForKey(array_element)).trim(),
						array_element);
			}
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(
			EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject "
					+ eoenterpriseobject + " is not in an EOEditingContext.");
		}
		if (eoenterpriseobject == null) {
			return null;
		} else {
			com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1
					.globalIDForObject(eoenterpriseobject);
			return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);
		}
	}

	public static A_FwkDTRecord createAndInsertInstance(
			EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	/**
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites
	 *            Un tableau d'objets {@link ISpecificite}. Ce tableau est
	 *            affecte a l'objet juste apres sa creation, avant l'insertion
	 *            dans l'editing context.
	 * @return
	 */
	public static A_FwkDTRecord createAndInsertInstance(
			EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription
				.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException(
					"Could not find EOClassDescription for entity name '" + s + "' !");
		} else {
			A_FwkDTRecord eoenterpriseobject = (A_FwkDTRecord) eoclassdescription
					.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	/**
	 * @return Une map avec en clé le nom de l'attribut et en valeur un integer
	 *         contenant la taille max de l'attribut.
	 */
	public Map attributesTaillesMax() {
		if (attributesTaillesMax == null) {
			attributesTaillesMax = attributeMaxSizesForEntityName(
					this.editingContext(), entityName());
		}
		return attributesTaillesMax;
	}

	public NSArray attributesObligatoires() {
		if (attributesObligatoires == null) {
			attributesObligatoires = requiredAttributeForEntityName(
					this.editingContext(), entityName());
		}
		return attributesObligatoires;
	}

	/**
	 * Vérife si les champs obligatoires sont bien saisis (en fonction de ce qui
	 * est indiqué dans le modèle)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesObligatoires()
			throws NSValidation.ValidationException {
		Iterator iterator = attributesObligatoires().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) == null) {
				throw new NSValidation.ValidationException("Le champ "
						+ getDisplayName(key) + " est obligatoire.");
			}
		}
	}

	/**
	 * Renvoie le nom d'affichage de la propriété. Utilise
	 * {@link #displayNames()} .
	 * 
	 * @param propertyName
	 *            nom de la propriété (attribut ou relation)
	 * @return
	 */
	public String getDisplayName(String propertyName) {
		if (displayNames().get(propertyName) != null) {
			return (String) displayNames().get(propertyName);
		}
		return propertyName;
	}

	/**
	 * Renvoie une Map contenant en clé le nom de la propriété et en valeur le
	 * nom d'affichage (parlant) de cette propriété. Par exemple <CP, Code
	 * Postal>. Par défaut la Map est vide, il faut surcharger la méthode.
	 */
	public Map displayNames() {
		return new HashMap();
	}

	/**
	 * Verifie la longueur maximale des champs saisis (a partir de la taille
	 * indiquée dans le modèle).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesLongueursMax()
			throws NSValidation.ValidationException {
		Iterator iterator = attributesTaillesMax().keySet().iterator();
		System.out.println(attributesTaillesMax().keySet());
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) != null
					&& ((String) valueForKey(key)).length() > ((Integer) attributesTaillesMax()
							.get(key)).intValue()) {
				throw new NSValidation.ValidationException("La taille du champ "
						+ getDisplayName(key) + " ne doit pas dépasser "
						+ ((Integer) attributesTaillesMax().get(key)).intValue()
						+ " caractères.");
			}
		}
	}

	/**
	 * @return true si l'objet possede un globalID temporaire (n'existe pas
	 *         encore dans la base de données).
	 */
	public boolean hasTemporaryGlobalID() {
		return (globalID() != null && globalID() instanceof EOTemporaryGlobalID);
	}

	/**
	 * @return le globalID de l'objet à partir de l'editingContext associé. Null
	 *         si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}

	public String dressExceptionMsg(Exception e) {
		return this.getClass().getSimpleName() + " : " + toDisplayString() + ":"
				+ e.getLocalizedMessage();
	}

	public String toDisplayString() {
		return toString();
	}

	public static final String executeStoredProcedureNamed = "executeStoredProcedureNamed";
	public static final String primaryKeyForObject = "primaryKeyForObject";
	public static final String rawRowsForSQL = "rawRowsForSQL";
	public static final String objectsWithFetchSpecificationAndBindings = "objectsWithFetchSpecificationAndBindings";
	public static final String requiredAttributesForObject = "requiredAttributesForObject";
	public static final String attributesMaxSizesForObject = "attributesMaxSizesForObject";

	public static NSDictionary primaryKeyForObject(EOEditingContext ec,
			EOEnterpriseObject eo) throws Exception {
		return EOUtilities.primaryKeyForObject(ec, eo);
	}

	public static NSArray rawRowsForSQL(EOEditingContext ec, String modelName,
			String sqlString, NSArray keys) throws Throwable {
		return EOUtilities.rawRowsForSQL(ec, modelName, sqlString, keys);
	}

	public static NSDictionary executeStoredProcedureNamed(EOEditingContext ec,
			String name, NSDictionary args) throws Throwable {
		return EOUtilities.executeStoredProcedureNamed(ec, name, args);
	}

	public static Map attributeMaxSizesForEntityName(EOEditingContext ec,
			String name) {

		HashMap res = new HashMap();

		EOClassDescription _classDescription = EOClassDescription
				.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";

		try {
			// Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width",
					new Class[] {});

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription
					.getDeclaredMethod("entity", new Class[] {

			});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod(
					"attributeNamed", new Class[] { String.class });

			Object entity = myEOClassDescription_entity.invoke(_classDescription,
					new Object[] {});

			Enumeration enumeration = _classDescription.attributeKeys()
					.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity,
						new Object[] { attName });

				Number width = (Number) myeoAttribute_width.invoke(object,
						new Object[] {});
				if (width != null && width.intValue() > 0) {
					res.put(attName, new Integer(width.intValue()));
				}
			}

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray requiredAttributeForEntityName(EOEditingContext ec,
			String name) {

		EOClassDescription _classDescription = EOClassDescription
				.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";

		try {
			// Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_allowsNull = myeoAttribute.getDeclaredMethod(
					"allowsNull", new Class[] {});

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription
					.getDeclaredMethod("entity", new Class[] {

			});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod(
					"attributeNamed", new Class[] { String.class });

			Object entity = myEOClassDescription_entity.invoke(_classDescription,
					new Object[] {});

			NSMutableArray res = new NSMutableArray();
			Enumeration enumeration = _classDescription.attributeKeys()
					.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity,
						new Object[] { attName });
				Boolean xxx = (Boolean) myeoAttribute_allowsNull.invoke(object,
						new Object[] {});
				if (!xxx.booleanValue()) {
					res.addObject(attName);
				}
			}
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray objectsWithFetchSpecificationAndBindings(
			EOEditingContext ec, String entityName, String fetchSpecName,
			NSDictionary bindings) throws Throwable {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, entityName,
				fetchSpecName, bindings);
	}

	public static NSArray objectsMatchingKeyAndValue(EOEditingContext ec,
			String entityName, String key, Object value) {
		NSDictionary dict = new NSDictionary(value, key);
		NSArray results = objectsMatchingValues(ec, entityName, dict);
		return results;
	}

	public static NSArray objectsMatchingValues(EOEditingContext ec, String name,
			NSDictionary values) {
		EOQualifier qualifier = EOQualifier.qualifierToMatchAllValues(values);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(name, qualifier,
				null);
		NSArray results = ec.objectsWithFetchSpecification(fetchSpec);
		return results;
	}

	/**
	 * @param editingContext
	 * @return L'editingContext de plus haut niveau (avant enregistrement dans
	 *         la base ). Renvoi null si editingContext est nul.
	 */
	public static EOEditingContext getTopLevelEditingContext(
			EOEditingContext editingContext) {
		if (editingContext == null) {
			return null;
		}
		EOEditingContext edc = editingContext;
		while (!(edc.parentObjectStore() instanceof EOObjectStoreCoordinator)) {
			edc = (EOEditingContext) edc.parentObjectStore();
		}
		return edc;
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	// recup depuis CktlRecord

	/* Les methode pour acceder aux valeurs */

	public Number numberForKey(String key) {
		return (Number) storedValueForKey(key);
	}

	public int intForKey(String key) {
		return numberForKey(key).intValue();
	}

	public String stringForKey(String key) {
		return (String) storedValueForKey(key);
	}

	public NSTimestamp dateForKey(String key) {
		return (NSTimestamp) storedValueForKey(key);
	}

	public boolean boolForKey(String key) {
		return StringCtrl.toBool(storedValueForKey(key).toString());
	}

	public NSArray arrayForKey(String key) {
		return (NSArray) valueForKey(key);
	}

	public EOEnterpriseObject eoForKey(String key) {
		return (EOEnterpriseObject) valueForKey(key);
	}

	public CktlRecord recForKey(String key) {
		return (CktlRecord) valueForKey(key);
	}

	/* Les methodes pour acceder aux valeurs via keyPath */

	public Number numberForKeyPath(String keyPath) {
		return (Number) valueForKeyPath(keyPath);
	}

	public int intForKeyPath(String keyPath) {
		return numberForKeyPath(keyPath).intValue();
	}

	public String stringForKeyPath(String keyPath) {
		return (String) valueForKeyPath(keyPath);
	}

	public NSTimestamp dateForKeyPath(String keyPath) {
		return (NSTimestamp) valueForKeyPath(keyPath);
	}

	public boolean boolForKeyPath(String keyPath) {
		return StringCtrl.toBool(valueForKeyPath(keyPath).toString());
	}

	public NSArray arrayForKeyPath(String key) {
		return (NSArray) valueForKeyPath(key);
	}

	public EOEnterpriseObject eoForKeyPath(String key) {
		return (EOEnterpriseObject) valueForKeyPath(key);
	}

	public CktlRecord recForKeyPath(String key) {
		return (CktlRecord) valueForKeyPath(key);
	}

	/* Les methodes qui permettent de formater les donnees */

	/**
	 * Retourne la valeur normalize de la valeur de l'attribut <code>key</code>.
	 * On suppose que la valeur de <code>key</code> est une chaine de
	 * caracteres. Si la valeur est <code>null</code>, alors elle est convertie
	 * en une chaine vide. Cette methode ne retourne donc jamais la valeur
	 * <code>null</code>.
	 */
	public String stringNormalizedForKey(String key) {
		return StringCtrl.normalize(stringForKey(key));
	}

	/**
	 * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et la
	 * convertie en une chaine de caracteres en suivant le format donne dans
	 * <code>formatter</code>.
	 */
	public String dateStringForKey(String key, String formatter) {
		return DateCtrl.dateToString(dateForKey(key), formatter);
	}

	/**
	 * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et la
	 * convertie en une chaine de caracteres. Le format de la date utilise est
	 * "%d/%m/%Y" (par exemple, 25/03/2004).
	 */
	public String dateStringForKey(String key) {
		return dateStringForKey(key, "%d/%m/%Y");
	}

	/**
	 * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et la
	 * convertie en une chaine de caracteres. Le format de la date utilise est
	 * "%d/%m/%Y %H:%M" (par exemple, 25/03/2004 13:35).
	 */
	public String dateTimeStringForKey(String key) {
		return dateStringForKey(key, "%d/%m/%Y %H:%M");
	}

	/**
	 * Retourne la longeur maximal en octets de la valeur autorisee pour
	 * l'attribut <code>attributeName</code> de cet enregistrement.
	 * 
	 * <p>
	 * Retourne -1 si l'attribut n'existe pas dans cet enregistrement ou s'il
	 * est de type numerique ou date.
	 * </p>
	 */
	public int maxLengthForAttribute(String attributeName) {
		return maxLengthForAttribute(entityName(), attributeName);
	}

	/*
	 * Les methodes permttant d'extraire les valeurs des attributs a partir des
	 * objets
	 */

	public static Object recordValueForKey(Object record, String key) {
		return ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static String recordStringForKey(Object record, String key) {
		return (String) ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static Number recordNumberForKey(Object record, String key) {
		return (Number) ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static int recordIntForKey(Object record, String key) {
		return ((Number) ((NSKeyValueCoding) record).valueForKey(key)).intValue();
	}

	public static NSTimestamp recordDateForKey(Object record, String key) {
		return (NSTimestamp) ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static boolean recordBoolForKey(Object record, String key) {
		String value = ((NSKeyValueCoding) record).valueForKey(key).toString();
		return StringCtrl.toBool(value.toUpperCase());
	}

	public static NSArray recordArrayForKey(Object record, String key) {
		return (NSArray) ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static EOEnterpriseObject recordEOForKey(Object record, String key) {
		return (EOEnterpriseObject) ((NSKeyValueCoding) record).valueForKey(key);
	}

	public static CktlRecord recordRecForKey(Object record, String key) {
		return (CktlRecord) ((NSKeyValueCoding) record).valueForKey(key);
	}

	/* D'autres methodes */
	/**
	 * Retourne l'objet correspondant a la valeur <code>null</code> dans
	 * l'implementation actuelle de EOF.
	 */
	public static Object nullValue() {
		// La version pour WO 5.x
		return EOKeyValueCoding.NullValue;
	}

	/**
	 * 
	 */
	public static Object valueIfNull(Object value) {
		if (value == null)
			return nullValue();
		return value;
	}

	/**
	 * Teste si la valeur donnee correspond a la valeur NULL. Elle l'est si
	 * <code>value</code> est egale a <code>null</code> ou
	 * {@link NSKeyValueCoding#NullValue}.
	 */
	public static boolean isNullValue(Object value) {
		return ((value == null) || (value == nullValue()));
	}

	/**
	 * Retourne la longeur maximal en octets de la valeur autorisee pour
	 * l'attribut <code>attributeName</code> de la table <code>tableName</code>.
	 * 
	 * <p>
	 * Retourne -1 si la table ou l'attribut n'existe pas ou si l'attribut est
	 * de type numerique ou date.
	 * </p>
	 */
	public static int maxLengthForAttribute(String tableName, String attributeName) {
		EOEntity entity = EOModelGroup.defaultGroup().entityNamed(tableName);
		if (entity != null) {
			EOAttribute attribute = entity.attributeNamed(attributeName);
			if ((attribute != null) && (attribute.width() > 0))
				return attribute.width();
		}
		return -1;
	}

	// methodes rajoutees de DTWeb (classe DTRecord)

	/**
	 * 
	 */
	public static Hashtable recordToHashtable(EOEnterpriseObject rec,
			boolean keepEmpty) {
		return keyValueToHashtable(rec, rec.attributeKeys(), keepEmpty);
	}

	/**
	 * 
	 */
	public static Hashtable dicoToHashtable(NSDictionary dico,
			boolean keepEmpty) {
		return keyValueToHashtable(dico, dico.allKeys(), keepEmpty);
	}

	/**
	 * 
	 */
	public static Hashtable keyValueToHashtable(NSKeyValueCoding dico,
			NSArray keys,
			boolean keepEmpty) {
		Hashtable result = new Hashtable();
		Object value;
		for (int i = 0; i < keys.count(); i++) {
			value = dico.valueForKey((String) keys.objectAtIndex(i));
			if (CktlRecord.isNullValue(value)) {
				if (keepEmpty)
					result.put(keys.objectAtIndex(i), "<null>");
			} else {
				result.put(keys.objectAtIndex(i), value);
			}
		}
		return result;
	}

}
