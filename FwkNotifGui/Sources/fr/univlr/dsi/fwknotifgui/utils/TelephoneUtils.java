package fr.univlr.dsi.fwknotifgui.utils;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.server.metier.NEOPersonneTelephone;
import fr.univlr.dsi.fwknotifgui.components.NotificationUtils;
import fr.univlr.dsi.fwknotifgui.enhanced.EOPersonneTelephoneEnhanced;

public class TelephoneUtils {

	private final static Logger log = Logger.getLogger(NotificationUtils.class);

	public static NSMutableArray<EOPersonneTelephoneEnhanced> fillTelephones(EOEditingContext ec, EOIndividu individu, INotifApplication application) {

		NSMutableArray<EOPersonneTelephoneEnhanced> telephoneEnhancedListe = new NSMutableArray<EOPersonneTelephoneEnhanced>();
		NSMutableArray<NEOPersonneTelephone> eoPersonneTelephoneListe = new NSMutableArray<NEOPersonneTelephone>();
		if (individu != null) {
			EOQualifier qual1 = EOPersonneTelephone.PERS_ID.eq(individu.persId());
			EOQualifier qual2 = EOPersonneTelephone.TYPE_NO.eq("MOB");
			EOQualifier qual3 = null;

			if (individu.isEtudiant()) {
				qual3 = EOPersonneTelephone.TYPE_TEL.eq("ETUD");

			}

			if (individu.isPersonnel()) {
				NSMutableArray<String> codes = new NSMutableArray<String>();
				codes.add("PRV");
				codes.add("PRF");
				qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
			}

			if ((individu.isEtudiant() && individu.isPersonnel()) || ((!individu.isEtudiant()) && (!individu.isPersonnel()))) {
				NSMutableArray<String> codes = new NSMutableArray<String>();
				codes.add("ETUD");
				codes.add("PRF");
				codes.add("PRV");
				qual3 = EOPersonneTelephone.TYPE_TEL.in(codes);
			}

			EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);
			
			eoPersonneTelephoneListe = new NSMutableArray<NEOPersonneTelephone>(NEOPersonneTelephone.fetchNPersonneTelephones(ec, qualifierFinale, null));
		}
		for(NEOPersonneTelephone eoPersonneTelephoneItem : eoPersonneTelephoneListe){
			
			telephoneEnhancedListe.add(new EOPersonneTelephoneEnhanced(ec, individu, eoPersonneTelephoneListe, eoPersonneTelephoneItem, application));
		}
		
		
		return new NSMutableArray<EOPersonneTelephoneEnhanced>(telephoneEnhancedListe);
	}
	
	
	public static NSMutableArray<NEOPersonneTelephone> fetchEOPersonneTelephones(EOEditingContext ec, EOIndividu individu) {

		NSMutableArray<NEOPersonneTelephone> eoPersonneTelephoneListe = new NSMutableArray<NEOPersonneTelephone>();
		
		EOQualifier qual = NEOPersonneTelephone.PERS_ID.eq(individu.persId());
		
		eoPersonneTelephoneListe.addAll(NEOPersonneTelephone.fetchNPersonneTelephones(ec, qual, null));
		
		
		return eoPersonneTelephoneListe;
	}
	
	
	
	public static String cleanStringNumber(String number) {

		if (number != null) {
			try {
				number = number.trim();
				number = number.replaceAll(" ", "");
				number = number.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "+");
				number = number.replaceAll("[A-z]", "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

//		if (number.equalsIgnoreCase("")) {
//			number = "0";
//		}

		return number;
	}
}
