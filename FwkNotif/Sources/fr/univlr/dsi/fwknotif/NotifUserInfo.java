package fr.univlr.dsi.fwknotif;

import org.apache.log4j.Logger;
//import org.cocktail.fwkcktlpersonne.common.metier.droits.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;

public class NotifUserInfo extends ApplicationUser {

public final static String C_STRUCTURE_ADMIN_KEY = "fr.univlr.dsi.notif.droit.cstructrureadmin";
	

	// droits globaux
	private static Boolean _isAdmin;
	private Boolean _isAccesAutorise;
	private EOIndividu eoIndividu;
	private final static Logger log = Logger.getLogger(NotifUserInfo.class);
	
	
	
	public NotifUserInfo(EOEditingContext ec, Integer persId) {
		super(ec, 113400);
//		super(ec, persId);
//		super(ec, 93616); // FORBEAU FRANCIS
//		super(ec, 333991); // BOUZIDI Kadir
//		super(ec, 2018); // philippe berger
//		super(ec, 3065); // cyril taraade
//		super(ec, 183058); // DILAN JOB taraade
//		super(ec, 296201); // FAYET SYLVIE
//		super(ec, 3055); // OGIER JEAN-MARC
//		super(ec, 61495); // PRIN RODOLPHE
//		super(ec, 87934); // WIART ISABELLE
//		super(ec, 24031); // Julien blandineau
//		super(ec, 389815); // PICOT SANDY --> doctorante
		
		

		persId = 113400;
		EOIndividu individu = EOIndividu.individuWithPersId(ec, persId);
		
		log.info("Connexion de : " + individu.getNomAndPrenom() + " (persId : " + persId + " - n°individu : " + individu.getNumero() + " - administrateur : " + isAdmin(persId)+")");
	}

	public final EOIndividu getEoIndividu() {
		if (eoIndividu == null) {
			eoIndividu = (EOIndividu) EOIndividu.individuWithPersId(getEditingContext(), getPersId());
		}
		return eoIndividu;
	}

	public final boolean isAdmin(Integer persId) {

		Boolean rep = false;

		rep = this.isMembre(C_STRUCTURE_ADMIN_KEY, persId);
		
		return rep;
	}
	
	
	public final boolean isRoot(Integer persId){
		Boolean rep = false;

		NSArray<String> roots = ERXProperties.arrayForKey("fr.univlr.dsi.notif.droit.root");
		
		for(String root : roots){
			if(root.equalsIgnoreCase(persId.toString())){
				rep = true;
			}
		}
		
		return rep;
	}
	
	
	public final boolean isAdmin() {

		Boolean rep = false;

		rep = this.isMembre(C_STRUCTURE_ADMIN_KEY, eoIndividu.persId());
		
		return rep;
	}

	public static Boolean getIsAdmin() {
		return _isAdmin;
	}

	public static Boolean isAdministrateur() {
		return getIsAdmin();
	}

	/**
	 * Il doit posséder au moins un droit sur 1 ec de l'année en cours ou etre
	 * admin
	 * 
	 * @return
	 */
	public final boolean isAccesAutorise() {
		if (_isAccesAutorise == null) {
			_isAccesAutorise = Boolean.FALSE;
			_isAccesAutorise = Boolean.TRUE;
		}
		return _isAccesAutorise.booleanValue();
	}

	/**
	 * Membre d'une structure
	 * 
	 * @param cStructureConfigKey
	 * @return
	 */
	private final boolean isMembre(String cStructureConfigKey, Integer persId) {
		
		boolean isMembre = false;

		NSArray<String> cStructures = ERXProperties.arrayForKey(C_STRUCTURE_ADMIN_KEY);
		
		for(String cStructure : cStructures){
			if (EOStructureForGroupeSpec.isPersonneInGroupe(EOIndividu.individuWithPersId(getEditingContext(), persId),
					EOStructure.structurePourCode(getEditingContext(), cStructure))) {
				isMembre = true;
			}
		}

		return isMembre;
	}
}
