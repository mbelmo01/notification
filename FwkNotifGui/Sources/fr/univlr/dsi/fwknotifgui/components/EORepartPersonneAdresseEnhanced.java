package fr.univlr.dsi.fwknotifgui.components;

import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.INotifSession;
import fr.univlr.dsi.fwknotif.NotificationEnhanced;
import fr.univlr.dsi.fwknotif.server.metier.EONotificationMailConf;
import fr.univlr.dsi.fwknotifgui.enhanced.EmailUpdateResponse;
import fr.univlr.dsi.fwknotifgui.utils.HtmlEmailSender;

public class EORepartPersonneAdresseEnhanced {

	private EORepartPersonneAdresse personneAdresse;
	private INotifSession session;
	private EOEditingContext ec;
	private CktlMailBus mailBus;
	private INotifApplication application;

	/**
	 * mailconf = entree dans ma table mail valide = derniere adresse valide
	 * connue dans le SI. Adresse connue dans mailConf mail à valider = adresse
	 * entrée dans le champs de saisie ; elle est en attente de validation
	 * repart mail = derniere adresse valide connue dans le SI. Extraite de
	 * EORepartAdresse hash : hash du code envoyé par mail à l'utilisateur. code
	 * : champs dans lequel l'utilisateur saisit le code qu'il a recu par mail.
	 * Son hash doit etre égale au hash de confMail
	 * 
	 * 
	 * displayRegisterButton : est-ce qu'on affiche le boutton "Enregistrer"
	 * displayConfirmButton : est-ce qu'on affiche le boutton "Confirmer";
	 * displayUpdateButton : est-ce qu'on affiche le boutton "Mettre à jour";
	 * displayAdresseEnCoursDeValidation : est-ce qu'on affiche le champs
	 * correspondant à l'adresse en attente de validation displayNotification :
	 * est-ce qu'on affiche une notification
	 */

	private final static Logger log = Logger.getLogger(EORepartPersonneAdresseEnhanced.class);

	private EONotificationMailConf mailConf = null;
	private String mailValide = null;
	private String mailAValider = null;
	private String repartMail = null;

	private String hash = null;
	private String code = null;

	public Boolean displayRegisterButton = false;
	public Boolean displayConfirmButton = false;
	public Boolean displayUpdateButton = false;
	public Boolean displayAdresseEnCoursDeValidation = false;
//	public Boolean displayNotification = false;
	private PreferencesUtils preferences = null;

	EmailUpdateResponse emailUpdateResponse = new EmailUpdateResponse();

	public EORepartPersonneAdresseEnhanced(EORepartPersonneAdresse personneAdresse, INotifSession session, 	INotifApplication application) {
		super();
		this.session = session;
		this.ec = session.defaultEditingContext();
		this.personneAdresse = personneAdresse;
		this.mailBus = session.mailBus();
		this.application = application;
		this.preferences = new PreferencesUtils(ec);

		if (personneAdresse.eMail() != null) {
			repartMail = personneAdresse.eMail();
		} else {
			repartMail = "";
		}

		this.mailConf = getMailConf();

		if (mailConf != null) {
			this.mailValide = mailConf.mailValide();
			this.mailAValider = mailConf.mailAValider();
		} else {
			while (mailConf == null) {
				mailConf = createNotificationMailConf();
			}

		}

		initForm();

		preferences.getDomainesInterdits();
	}

	public EONotificationMailConf createNotificationMailConf() {
		EONotificationMailConf conf = null;
		try {
			conf = EONotificationMailConf.createNotificationMailConf(ec, personneAdresse.adrOrdre(),
					personneAdresse.persId(), personneAdresse.tadrCode());
			NotificationUtils.save(ec);
		} catch (Exception e) {
			String message = "Impossible de creer l'entree NotificationMailConf en Base de données.";

			try {
				// throw new NotificationException(personneAdresse.toIndividu(),
				// session.mailBus(), e, message);
				throw new NotificationException(personneAdresse.toIndividu(), e, message);
			} catch (NotificationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return conf;
	}

	private void initForm() {

		setButtonFalse();

		if (personneAdresse.eMail() == null || personneAdresse.eMail().equalsIgnoreCase("")
				|| personneAdresse.eMail().equalsIgnoreCase(mailAValider)) {

			displayRegisterButton = true;
			this.repartMail = "";

		} else {

			this.repartMail = personneAdresse.eMail();
			if (personneAdresse.eMail().equalsIgnoreCase(mailValide)) {
				displayUpdateButton = true;

				// cette instruction a été ajouté apres tous les tests effectués
				displayAdresseEnCoursDeValidation = false;
			} else {
				displayConfirmButton = true;
			}
		}

		if (mailAValider != null) {
			{
				/*
				 * ajout personnel pour savoir si on affiche l'adresse à valider
				 * on l'affiche lorsque le champs "a valider" n'est pas null
				 */
				displayAdresseEnCoursDeValidation = true;
			}
		}

	}

	private void setButtonFalse() {
		displayRegisterButton = false;
		displayConfirmButton = false;
		displayUpdateButton = false;
		displayAdresseEnCoursDeValidation = false;
	}

	public EORepartPersonneAdresse getPersonneAdresse() {
		return personneAdresse;
	}

	public void setPersonneAdresse(EORepartPersonneAdresse personneAdresse) {
		this.personneAdresse = personneAdresse;
	}

	public Boolean isChecked() {

		Boolean rep = false;

		if (mailConf == null) {
			mailConf = getMailConf();
		}

		if (mailConf != null) {
			if (repartMail.equalsIgnoreCase(mailValide) && mailAValider == null) {
				rep = true;
			}
		}

		return rep;
	}

	public EONotificationMailConf getMailConf() {

		EOQualifier qual1 = EONotificationMailConf.MAIL_CONF_PERS_ID.eq(personneAdresse.persId());
		EOQualifier qual2 = EONotificationMailConf.MAIL_CONF_TADR_CODE.eq(personneAdresse.tadrCode());
		EOQualifier qual3 = EONotificationMailConf.MAIL_CONF_ADR_ORDRE.eq(personneAdresse.adrOrdre());
		EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3);
		this.mailConf = EONotificationMailConf.fetchNotificationMailConf(ec, qualifierFinale);

		return this.mailConf;
	}

	public String getMailValide() {
		return mailValide;
	}

	public void setMailValide(String mailValide) {
		this.mailValide = mailValide;
	}

	public String getMailAValider() {
		return mailAValider;
	}

	public void setMailAValider(String mailAValider) {
		this.mailAValider = mailAValider;
	}

	public String getRepartMail() {
		return repartMail;
	}

	public void setRepartMail(String repartMail) {
		this.repartMail = repartMail;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMailConf(EONotificationMailConf mailConf) {
		this.mailConf = mailConf;
	}

	public Boolean getDisplayRegisterButton() {
		return displayRegisterButton;
	}

	public void setDisplayRegisterButton(Boolean displayRegisterButton) {
		this.displayRegisterButton = displayRegisterButton;
	}

	public Boolean getDisplayConfirmButton() {
		return displayConfirmButton;
	}

	public void setDisplayConfirmButton(Boolean displayConfirmButton) {
		this.displayConfirmButton = displayConfirmButton;
	}

	public Boolean getDisplayUpdateButton() {
		return displayUpdateButton;
	}

	public void setDisplayUpdateButton(Boolean displayUpdateButton) {
		this.displayUpdateButton = displayUpdateButton;
	}

	public Boolean getDisplayAdresseEnCoursDeValidation() {
		return displayAdresseEnCoursDeValidation;
	}

	public void setDisplayAdresseEnCoursDeValidation(Boolean displayAdresseEnCoursDeValidation) {
		this.displayAdresseEnCoursDeValidation = displayAdresseEnCoursDeValidation;
	}

//	public Boolean getDisplayNotification() {
//		return displayNotification;
//	}
//
//	public void setDisplayNotification(Boolean displayNotification) {
//		this.displayNotification = displayNotification;
//	}

	public void update() {

		if (emailDomaineAutorise(this.repartMail)) {
			if (this.repartMail != null && this.mailConf != null && (!this.repartMail.equalsIgnoreCase(mailConf.mailValide()))) {
				try {

					log.info(this.personneAdresse.toIndividu().getNomAndPrenom() + "("+ this.personneAdresse.toIndividu().noIndividuLogin() + ")" + " met à jour son adresse email.");
					log.info("Ancienne adresse : " + this.personneAdresse.eMail());
					log.info("Nouvelle adresse : " + this.getRepartMail());

					this.mailConf.setMailAValider(this.repartMail);
					this.mailConf.setMailConfCodeHash(hash);
					this.mailConf.setDModification(new NSTimestamp());
					this.mailConf.setMailConfValideStatus("N");
					NotificationUtils.save(ec);
					resendEmail();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String generateCode() {
		String str = "";

		UUID uuid = UUID.randomUUID();
		str = uuid.toString().substring(0, 6);
		return str;
	}

	public String codage(String code) {
		byte[] encodedBytes = Base64.encodeBase64(code.getBytes());

		return new String(encodedBytes);
	}

	/*
	 * fonction utilisée pour renvoyer le mail de confirmation. Elle génére un
	 * nouveau token et le renvoie à l'adresse "à valider"
	 */
	public WOActionResults resendEmail() {

		if (mailConf == null) {
			mailConf = getMailConf();
		}

		try {

			String codeValue = generateCode();
			this.hash = codage(codeValue);
			mailConf.setMailConfCodeHash(hash);
			
			
			UUID uuid = UUID.randomUUID();
			Integer getNb_days_lifetime_link = Integer.parseInt(preferences.getNb_days_lifetime_link().stringContent());

			this.mailConf.setUuid(uuid.toString());
			this.mailConf.setDModification(new NSTimestamp());
			this.mailConf.setUuidDDebut(new NSTimestamp());
			this.mailConf.setUuidDFin(new NSTimestamp().timestampByAddingGregorianUnits(0, 0, getNb_days_lifetime_link, 0, 0, 0));

			String from = preferences.getFrom().stringContent();
			String to = mailConf.mailAValider();
			String cc = preferences.getCc().stringContent();
			String subject = preferences.getSubject().stringContent();
			String message = insertInformation(preferences.getMail().stringContent(), mailConf.uuid(), codeValue);

			HtmlEmailSender.sendHtmlEmailWithDefaultParams(from, to, cc, subject, message);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			NotificationUtils.save(ec);
		} catch (NotificationException e) {
			e.printStackTrace();
		}

		return null;
	}

	/*
	 * permet de remplir le template avec les valeurs qui vont bien token
	 * adresse de l'appli nombre de jour de validité du code nom et prenm du
	 * destinataire
	 */
	public String insertInformation(String message, String uuid, String code) {

		try {

			String url = application.getApplicationURL(session.context());

			url += "/wa/DAMailValidation?uuid=" + uuid;

			message = message.replace("_code_", code);
			message = message.replace("_url_token_", url);
			message = message.replace("_recipient_nom_prenom_", personneAdresse.toIndividu().getNomAndPrenom());
			message = message.replace("_nb_days_lifetime_link_",
					preferences.getNb_days_lifetime_link().stringContent());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}

	/*
	 * determine si un mail (notamment son domaine) est valide
	 */
	private Boolean emailDomaineAutorise(String email) {
		Boolean rep = true;

		if (email != null) {
			for (String dom : preferences.getDomainesInterdits()) {
				System.out.println("Domaine = " + dom.trim());
				if (email.contains(dom.trim())) {
					rep = false;
					application.getNotificationStore().addNotification(new NotificationEnhanced("error", "Nom de domaine non autorisé", "Le nom de domaine " + dom.trim() + " n'est pas autorisé", null, null, null));
				}
			}
		}

		return rep;
	}

	/*
	 * permet de supprimer le mail valide
	 */
	public void deleteEmail() {
		try {
			personneAdresse.setEMail(null);
			mailConf.setDModification(new NSTimestamp());
			mailConf.setMailValide(null);
			NotificationUtils.save(ec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * permet de supprimer le mail en cours de validation
	 */
	public void deleteEmailEnCours() {
		try {
			mailConf.setMailAValider(null);
			mailConf.setDModification(new NSTimestamp());
			NotificationUtils.save(ec);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * determine s'il faut afficher le boutton permettant de supprimer un mail
	 * valide. oui sauf si l'adresse mail dans repartAdresse est vide
	 */
	public Boolean displayDeleteButton() {
		Boolean rep = true;

		if (this.repartMail == null || this.repartMail.equalsIgnoreCase("")) {
			rep = false;
		}

		return rep;
	}

	/*
	 * permer de valider une adresse avec le code. Si le hash du code est bon,
	 * on copie l'adresse en cours de validation dans repartAdresse et dans
	 * mailValide
	 * 
	 * @return
	 */
	public Boolean validerAdresse() {

		Boolean rep = false;
		if (code != null) {

			try {

				if (codage(code).equalsIgnoreCase(mailConf.mailConfCodeHash())) {
					rep = validation(mailConf.uuid());
				} else {
					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.MailCodeValidationError);
					application.getNotificationStore().addNotification(notificationEnhanced);
				}
			} catch (Exception e) {
			}
		}

		return rep;
	}

	/*
	 * Permet de valider toute adresse correspondant à ce hash.
	 */
	public Boolean validation(String uuid) {

		Boolean rep = false;
		EOQualifier qual1 = EOIndividu.PERS_ID.eq(mailConf.mailConfPersId());

		if (mailConf != null) {
			EOQualifier qual2 = EORepartPersonneAdresse.ADR_ORDRE.eq(mailConf.mailConfAdrOrdre());
			EOQualifier qual3 = EORepartPersonneAdresse.TADR_CODE.eq(mailConf.mailConfTadrCode());
			EOQualifier qual4 = EORepartPersonneAdresse.RPA_VALIDE.eq("O");

			EOQualifier qualifierFinale = ERXQ.and(qual1, qual2, qual3, qual4);

			EORepartPersonneAdresse add = EORepartPersonneAdresse.fetchByQualifier(ec, qualifierFinale);

			if (mailConf.uuidDFin().after(new NSTimestamp())) {
				try {
					if (mailConf.mailAValider() != null) {
						add.setEMail(mailConf.mailAValider());
						mailConf.setMailValide(mailConf.mailAValider());
						mailConf.setMailAValider(null);
						mailConf.setMailConfValideStatus("O");
						mailConf.setDModification(new NSTimestamp());
						rep = true;
						application.getNotificationStore()
								.addNotification(new NotificationEnhanced(NotificationEnhanced.UpdateMailSuccess));
					}

					NotificationUtils.save(ec);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return rep;
	}
}