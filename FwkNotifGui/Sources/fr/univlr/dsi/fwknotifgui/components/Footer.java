package fr.univlr.dsi.fwknotifgui.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import fr.univlr.dsi.fwknotifgui.ctrl.FooterCtrl;

public class Footer extends ANotifComponent {
	

	private final static String BND_ctrl = "ctrl";

	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		// remonter le controleur aux bindings
		setValueForBinding(getCtrl(), BND_ctrl);
	}

	@Override
	public Class<?> getClassCtrl() {
		return FooterCtrl.class;
	}

	public FooterCtrl getCtrl() {
		return (FooterCtrl) super.getCtrl();
	}

	public Footer(WOContext context) {
		super(context);

		
	}

}