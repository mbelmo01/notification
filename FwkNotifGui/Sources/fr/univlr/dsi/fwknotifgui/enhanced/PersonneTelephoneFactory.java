package fr.univlr.dsi.fwknotifgui.enhanced;

import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import fr.univlr.dsi.fwknotif.INotifApplication;
import fr.univlr.dsi.fwknotif.NotificationEnhanced;
import fr.univlr.dsi.fwknotif.server.metier.NEOPersonneTelephone;
import fr.univlr.dsi.fwknotifgui.components.NotificationException;
import fr.univlr.dsi.fwknotifgui.components.NotificationUtils;
import fr.univlr.dsi.fwknotifgui.utils.TelephoneUtils;

public class PersonneTelephoneFactory {

	private EOEditingContext ec = null;
	private EOIndividu eoIndividu = null;
	private INotifApplication application = null;

	private String noTelephone = "";
	private NSArray<EOPaysIndicatif> eoPaysIndicatifListe = new NSMutableArray<EOPaysIndicatif>();
	private EOPaysIndicatif eoPaysIndicatifItem = null;
	private EOPaysIndicatif eoPaysIndicatifSelection = null;

	private NSMutableArray<EOTypeTel> eoTypeTelListe = new NSMutableArray<EOTypeTel>();
	private EOTypeTel eoTypeTelItem = null;
	private EOTypeTel eoTypeTelSelection = null;

	private NEOPersonneTelephone phone = null;
	private Boolean isTelPrincipal = false;

	NSMutableArray<EOPersonneTelephoneEnhanced> eoPersonneTelephoneListe = new NSMutableArray<EOPersonneTelephoneEnhanced>();

	public PersonneTelephoneFactory(EOEditingContext edc, EOIndividu individu, INotifApplication appli) {
		super();
		ec = edc;
		eoIndividu = individu;
		application = appli;
		initEOPaysList();
		initEOTypeTelList();
//		eoPersonneTelephoneListe = TelephoneUtils.fillTelephones(ec, eoIndividu, appli);
	}

	public NEOPersonneTelephone getPhone() {
		return phone;
	}

	public void setPhone(NEOPersonneTelephone phone) {
		this.phone = phone;
	}

	public String getNoTelephone() {
		return noTelephone;
	}

	public void setNoTelephone(String noTelephone) {
		this.noTelephone = noTelephone;
	}

	public NSArray<EOPaysIndicatif> getEoPaysIndicatifListe() {
		return eoPaysIndicatifListe;
	}

	public void setEoPaysIndicatifListe(NSArray<EOPaysIndicatif> eoPaysIndicatifListe) {
		this.eoPaysIndicatifListe = eoPaysIndicatifListe;
	}

	public EOPaysIndicatif getEoPaysIndicatifItem() {
		return eoPaysIndicatifItem;
	}

	public void setEoPaysIndicatifItem(EOPaysIndicatif eoPaysIndicatifItem) {
		this.eoPaysIndicatifItem = eoPaysIndicatifItem;
	}

	public EOPaysIndicatif getEoPaysIndicatifSelection() {
		return eoPaysIndicatifSelection;
	}

	public void setEoPaysIndicatifSelection(EOPaysIndicatif eoPaysIndicatifSelection) {
		this.eoPaysIndicatifSelection = eoPaysIndicatifSelection;
	}

	public NSArray<EOTypeTel> getEoTypeTelListe() {
		return eoTypeTelListe;
	}

	public void setEoTypeTelListe(NSMutableArray<EOTypeTel> eoTypeTelListe) {
		this.eoTypeTelListe = eoTypeTelListe;
	}

	public EOTypeTel getEoTypeTelItem() {
		return eoTypeTelItem;
	}

	public void setEoTypeTelItem(EOTypeTel eoTypeTelItem) {
		this.eoTypeTelItem = eoTypeTelItem;
	}

	public EOTypeTel getEoTypeTelSelection() {
		return eoTypeTelSelection;
	}

	public void setEoTypeTelSelection(EOTypeTel eoTypeTelSelection) {
		this.eoTypeTelSelection = eoTypeTelSelection;
	}

	public Boolean getIsTelPrincipal() {
		System.out.println("isTelPrincipalFactory : " + isTelPrincipal);
		return isTelPrincipal;
	}

	public void setIsTelPrincipal(Boolean isTelPrincipal) {
		System.out.println("isTelPrincipalFactory : " + isTelPrincipal);
		this.isTelPrincipal = isTelPrincipal;
	}

	public void resertPersonneTelephoneFacotry() {
		initEOTypeTelList();
		initEOPaysList();
		noTelephone = "";
	}

	public void initEOPaysList() {

		// choix du pays indicatif par defaut
		eoPaysIndicatifListe = new NSArray<EOPaysIndicatif>();
		eoPaysIndicatifItem = null;
		eoPaysIndicatifSelection = null;
		try {
			eoPaysIndicatifListe = EOPaysIndicatif.fetchAll(ec);
			eoPaysIndicatifListe.sort(Comparator.comparing(EOPaysIndicatif::indicatif));
			eoPaysIndicatifSelection = getPaysIndicatifByIndicatif(33);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initEOTypeTelList() {

		// choix du pays indicatif par defaut
		eoTypeTelListe = new NSMutableArray<EOTypeTel>();
		eoTypeTelItem = null;
		eoTypeTelSelection = null;

		EOQualifier qual = null;
		try {

			if (eoIndividu.isEtudiant() && eoIndividu.isPersonnel()) {
				qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("PRV", "PRF", "ETUD"));
			} else {
				if (eoIndividu.isEtudiant()) {
					qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("ETUD"));
				}

				if (eoIndividu.isPersonnel()) {
					qual = EOTypeTel.C_TYPE_TEL.in(new NSArray<String>("PRV", "PRF"));
				}
			}
			eoTypeTelListe.addAll(EOTypeTel.fetchAll(ec, qual, null));
			eoTypeTelListe.sort(Comparator.comparing(EOTypeTel::cTypeTel));
			eoTypeTelSelection = eoTypeTelListe.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EOPaysIndicatif getPaysIndicatifByIndicatif(Integer indicatif) {
		EOPaysIndicatif pays = null;

		try {
			EOQualifier qual = EOPaysIndicatif.INDICATIF.eq(indicatif);
			pays = EOPaysIndicatif.fetchFirstByQualifier(ec, qual);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pays;

	}

	public String indicatifWithCountryName() {
		String str = eoPaysIndicatifItem.indicatif().toString();

		if (eoPaysIndicatifItem != null) {
			try {
				str += " -- " + eoPaysIndicatifItem.toPays().llPays();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return str;
	}

	

	
	
	
	
	
	
	
	
	public WOActionResults addPersonneTelephone() {

		noTelephone = TelephoneUtils.cleanStringNumber(noTelephone);
		if (noTelephone != null && (!noTelephone.equalsIgnoreCase(""))) {
			if (canBeAdded(eoTypeTelSelection.cTypeTel(), eoPaysIndicatifSelection.indicatif(), noTelephone)) {
				createAndInsertPhone();
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.AddPhoneSuccess);
				application.getNotificationStore().addNotification(notificationEnhanced);
			} else {
				System.out.println("NOOOOOOOOOOOOOOOOOON !!!!!!");
			}
		}
		return null;
	}
	
	public void createAndInsertPhone(){
		String telPrincipal = "N";
		
		if(isTelPrincipal){
			telPrincipal = "O";
		}
		
		phone = NEOPersonneTelephone.createNPersonneTelephone(ec, new NSTimestamp(), new NSTimestamp(), noTelephone, eoIndividu.persId(), telPrincipal, "MOB", eoTypeTelSelection.cTypeTel());
		phone.setIndicatif(eoPaysIndicatifSelection.indicatif());
		phone.setListeRouge("N");
		phone.setDDebVal(new NSTimestamp());
		Boolean rep = false;
		try {
			rep = NotificationUtils.save(ec);
		} catch (NotificationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(rep == true){
			resertPersonneTelephoneFacotry();
		}
	}

	/*
	 * verifie si un numéro peut être ajouté. Elle verifie si 
	 *  - le numéro est un doublon
	 *  - le numéro n'a pas la bonne longueur
	 *  - le numéro n'a pas le bon préfixe
	 */
	public Boolean canBeAdded(String typeTel, Integer indicatif, String numero) {
		Boolean rep = false;
		Boolean doublons = false;
		Boolean longueur = false;
		Boolean prefixe = false;

		doublons = verifierDoublon(typeTel, numero);
		longueur = verifierLongueur(eoPaysIndicatifSelection.indicatif(), numero);
		prefixe = verifierPrefixe(eoPaysIndicatifSelection.indicatif(), numero);

		if ((!doublons) && longueur && prefixe) {
			rep = true;
		}

		return rep;
	}

	/*
	 * verifie si un numero de telephone est doublon. Pour etre un doublon, il faut présenter la même combinaison type + numéro. (de toute facon y a une contrainte en BDD qui emepche cette insertion).
	 */
	public Boolean verifierDoublon(String typeTel, String numero) {
		Boolean rep = false;
		eoPersonneTelephoneListe = TelephoneUtils.fillTelephones(ec, eoIndividu, application);
		for (EOPersonneTelephoneEnhanced eoPersonneTelephone : eoPersonneTelephoneListe) {
			if (numero.equalsIgnoreCase(eoPersonneTelephone.getEoPersonneTelephoneItem().noTelephone())) {
				if (eoTypeTelSelection.cTypeTel().equalsIgnoreCase(eoPersonneTelephone.getEoPersonneTelephoneItem().typeTel())) {
					NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.DuplicateTypeAndPhone);
					application.getNotificationStore().addNotification(notificationEnhanced);
					rep = true;
				}
			}
		}

		return rep;
	}
	
	/*
	 * verifie que la longueur d'un numéro est correcte. Elle est entre 9 et 13 pour un numéro francais et 3 et 20 pour un numéro étranger. 
	 */
	public Boolean verifierLongueur(Integer indicatif, String numero) {
		Boolean rep = false;

		if (indicatif == 33) {
			if (numero.length() >= 9 && numero.length() <= 13) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.FrenchPhoneLenght);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		} else {
			if (numero.length() >= 3 && numero.length() <= 20) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.PhoneLenght);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		}
		return rep;
	}
	
	/*
	 * verifie si un numero commence bien par le bon prefixe : notamment en france, 06 ou 07 pour un mobile.
	 */
	public Boolean verifierPrefixe(Integer indicatif, String numero) {
		Boolean rep = false;

		String prefixe = numero.substring(0, 2);

		if (indicatif == 33) {
			if (prefixe.equalsIgnoreCase("06") || prefixe.equalsIgnoreCase("07")) {
				rep = true;
			} else {
				NotificationEnhanced notificationEnhanced = new NotificationEnhanced(NotificationEnhanced.FrenchPhonePrefixe);
				application.getNotificationStore().addNotification(notificationEnhanced);

			}
		} else {
			rep = true;
		}

		return rep;
	}

}
