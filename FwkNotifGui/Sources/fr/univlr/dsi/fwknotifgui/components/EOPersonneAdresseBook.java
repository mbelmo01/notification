package fr.univlr.dsi.fwknotifgui.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;

import com.webobjects.foundation.NSMutableArray;

public class EOPersonneAdresseBook {

	private EOIndividu individu = null;

	private NSMutableArray<Adresse> adresses = null;

	public EOPersonneAdresseBook(EOIndividu individu) {
		this.individu = individu;
		this.adresses = new NSMutableArray<Adresse>();
	}

	public EOIndividu getIndividu() {
		return individu;
	}

	public void setIndividu(EOIndividu individu) {
		this.individu = individu;
	}

	public NSMutableArray<Adresse> getAdresses() {
		return adresses;
	}

	public void setAdresses(NSMutableArray<Adresse> adresses) {
		this.adresses = adresses;
	}

	public void addAdresse(Adresse add) {
		adresses.add(add);
	}
}
